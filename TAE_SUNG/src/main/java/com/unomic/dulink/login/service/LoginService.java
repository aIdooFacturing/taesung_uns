package com.unomic.dulink.login.service;

import java.util.List;

import com.unomic.dulink.login.domain.LoginVo;
import com.unomic.dulink.login.domain.NcModelVo;

public interface LoginService {
	public List<NcModelVo> getNcManufacture();
	public LoginVo submit(String id, String password);
	public String getNCList() throws Exception;
	public String login(LoginVo loginVo) throws Exception;
	public String join(LoginVo loginVo) throws Exception;
};
