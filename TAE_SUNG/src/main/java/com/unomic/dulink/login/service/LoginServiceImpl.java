package com.unomic.dulink.login.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.login.domain.LoginVo;
import com.unomic.dulink.login.domain.NcModelVo;

@Service
@Repository
public class LoginServiceImpl implements LoginService{

	private final static String namespace= "com.dulink.login.";
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	
	@Override
	public List<NcModelVo> getNcManufacture() {
		List<NcModelVo> modelList = sql_ma.selectList(namespace+"getModels");
		return modelList;
	}

	@Override
	public LoginVo submit(String id, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNCList() throws Exception {
		List <NcModelVo> NCList = sql_ma.selectList(namespace + "getModels");
		List list = new ArrayList();
		for(int i = 0; i < NCList.size(); i++){
			Map map = new HashMap();
			map.put("id", NCList.get(i).getId());
			map.put("manufacture", NCList.get(i).getManufacture());
			map.put("model", NCList.get(i).getModel());
			map.put("type", NCList.get(i).getType());
			
			list.add(map);
		};
		
		String rtn = "";
		Map NCMap = new HashMap();
		NCMap.put("NCList", list);
		
		ObjectMapper om = new ObjectMapper();
		rtn = om.defaultPrettyPrintingWriter().writeValueAsString(NCMap);
		
		return rtn;
	}

	@Override
	public String login(LoginVo loginVo) throws Exception {
		LoginVo str = (LoginVo) sql_ma.selectOne(namespace + "login", loginVo);
		String rtn;
		if(str==null){
			rtn = "fail";
		}else{
			rtn = str.getCompany();
		};
		
		return rtn;
	}

	@Override
	public String join(LoginVo loginVo) throws Exception {
		String result = "";
		try{
			//add user
			sql_ma.insert(namespace + "join", loginVo);
			
			//add user_nc
			String[] NCList = loginVo.getNC().split(",");
			if(NCList.length!=1){
				for(int i = 0; i < NCList.length; i++){
					loginVo.setNCId(Integer.parseInt(NCList[i]));
					sql_ma.insert(namespace + "addUserNC", loginVo);
				};
			}
			
			result = "success";
		}catch(Exception e){
			result = "fail";
			if(e.getMessage().indexOf("Duplicate")!=-1){
				result = "dupl";
			};
		};
		
		return result;
	};
}
