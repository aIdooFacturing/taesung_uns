package com.unomic.dulink.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unomic.dulink.demo.service.DemoService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/demo")
@Controller
public class DemoController {
	
	private static final Logger logger = LoggerFactory.getLogger(DemoController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private DemoService demoService;
	
	@RequestMapping(value="gauge")
	public String chart2(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "demo/gauge";
	};
	
	@RequestMapping(value="gauge2")
	public String gauge2(HttpServletRequest request, ModelMap model, HttpSession session){
		String ip = request.getParameter("ip");
		String port = request.getParameter("port");

		session.setAttribute("ip", ip);
		session.setAttribute("port", port);
		return "demo/gauge2";
	};
	
@RequestMapping(value = "video4")
	
    public String video4() {

		return "test/video4";
    }
	
}
