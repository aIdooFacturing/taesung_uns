package com.unomic.dulink.mtc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.mtc.domain.AlarmEvtVo;
import com.unomic.dulink.mtc.domain.AlarmVo;
import com.unomic.dulink.mtc.domain.CycleVo;
import com.unomic.dulink.mtc.domain.SerialVo;
import com.unomic.factory911.adapter.domain.AdapterVo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MtcShooter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MtcShooter.class);
	
	private HashMap<String,SerialVo> mapSrl;
	private HashMap<String,AlarmEvtVo> mapAlamEvt;
	private HashMap<String,CycleVo> mapCycleEvt;

	public MtcShooter(){
		mapSrl = new HashMap<String, SerialVo>();
		mapAlamEvt = new HashMap<String, AlarmEvtVo>();
		mapCycleEvt = new HashMap<String, CycleVo>();
	}
	
	public void  inputSerial(AdapterVo inputVo) throws Exception{
		LOGGER.info("in setSendVo");
		LOGGER.info("inputVo:"+inputVo.toString());
		SerialVo tmpVo = new SerialVo();
		tmpVo.setMandt("TEST_BK");
		tmpVo.setMcCd(inputVo.getDvcId()+"");
		tmpVo.setMsrDt(CommonFunction.getNowUTC());
		tmpVo.setCrtPgmBlc(inputVo.getLine()+"");
		tmpVo.setMod(inputVo.getMode()+"");
		tmpVo.setStt(inputVo.getStatus()+"");
		tmpVo.setCrtPgmNm(inputVo.getMainPrgmName());
		tmpVo.setSpdLd(inputVo.getSpdLd()+"");
		tmpVo.setActFd(inputVo.getActFd()+"");
		tmpVo.setActSpdSp(inputVo.getSpdActSpeed()+"");
		tmpVo.setRegdt(CommonFunction.getNowUTC());

		this.getMapSrl().put(inputVo.getDvcId(), tmpVo);
	}
	
	public void  inputCycleEvt(AdapterVo inputVo) throws Exception{
		LOGGER.info("in inputCycleEvt");
		LOGGER.info("inputVo:"+inputVo.toString());
		CycleVo tmpVo = new CycleVo();
		
		tmpVo.setMnPgmStDt(inputVo.getMainPrgmStartTime2());
		tmpVo.setTtlPrgBlc(inputVo.getTotalLineNumber());
		tmpVo.setMnPgmNm(inputVo.getMainPrgmName());
		tmpVo.setMnPgmHd(inputVo.getPrgmHead());
		tmpVo.setPartCnt(inputVo.getPartCount());
		tmpVo.setRegdt(CommonFunction.getNowUTC());
		
		this.getMapCycleEvt().put(inputVo.getDvcId(), tmpVo);
		
	}
	
	//이후에는 pmc, nc, 구별해야함.
	public void inputAlarmEvt(AdapterVo inputVo){
		LOGGER.info("in setAlarmEvt");
		LOGGER.info("inputVo:"+inputVo.toString());
		
		AlarmEvtVo rtnEvtVo = new AlarmEvtVo();
		rtnEvtVo.setMcCd(inputVo.getDvcId()+"");
		rtnEvtVo.setRegdt(CommonFunction.getNowUTC());
		
		int ncAlarmCnt=0;
		int pmcAlarmCnt=0;
		List<AlarmVo> listNcAlarm= new ArrayList<AlarmVo>();
		List<AlarmVo> listPmcAlarm= new ArrayList<AlarmVo>();
		
		AlarmVo tmpAlarmVo = new AlarmVo();
		
		if(! (inputVo.getAlarmNum1() == null || inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum1());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg1());
			listNcAlarm.add(tmpAlarmVo);
		}
		if(! (inputVo.getAlarmNum2() == null || inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum2());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg2());
			listNcAlarm.add(tmpAlarmVo);
		}
		
		if(! (inputVo.getAlarmNum3() == null || inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum3());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg3());
			listNcAlarm.add(tmpAlarmVo);
		}
		
		ncAlarmCnt = listNcAlarm.size();
		rtnEvtVo.setCntNc(ncAlarmCnt);
		rtnEvtVo.setLstNc(listNcAlarm);
		
		LOGGER.info("rtnEvtVo:"+rtnEvtVo);
		
		this.mapAlamEvt.put(rtnEvtVo.getMcCd(), rtnEvtVo);
	}
	
	public void inputCycltEvt(AdapterVo inputVo){
		LOGGER.info("in inputCycltEvt");
		LOGGER.info("inputVo:"+inputVo.toString());
		
		AlarmEvtVo rtnEvtVo = new AlarmEvtVo();
		rtnEvtVo.setMcCd(inputVo.getDvcId()+"");
		rtnEvtVo.setRegdt(CommonFunction.getNowUTC());
		
		int ncAlarmCnt=0;
		int pmcAlarmCnt=0;
		List<AlarmVo> listNcAlarm= new ArrayList<AlarmVo>();
		List<AlarmVo> listPmcAlarm= new ArrayList<AlarmVo>();
		
		AlarmVo tmpAlarmVo = new AlarmVo();
		
		if(! (inputVo.getAlarmNum1() == null || inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum1());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg1());
			listNcAlarm.add(tmpAlarmVo);
		}
		if(! (inputVo.getAlarmNum2() == null || inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum2());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg2());
			listNcAlarm.add(tmpAlarmVo);
		}
		
		if(! (inputVo.getAlarmNum3() == null || inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL))){
			tmpAlarmVo.setCd(inputVo.getAlarmNum3());
			tmpAlarmVo.setMsg(inputVo.getAlarmMsg3());
			listNcAlarm.add(tmpAlarmVo);
		}
		
		ncAlarmCnt = listNcAlarm.size();
		rtnEvtVo.setCntNc(ncAlarmCnt);
		rtnEvtVo.setLstNc(listNcAlarm);
		
		LOGGER.info("rtnEvtVo:"+rtnEvtVo);
		
		this.mapAlamEvt.put(rtnEvtVo.getMcCd(), rtnEvtVo);
	}
}
