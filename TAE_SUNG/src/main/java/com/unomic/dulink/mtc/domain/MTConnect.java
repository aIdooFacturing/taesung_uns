package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MTConnect {
	private String name;
	private String creationTime;
	private String sender;

	private String actFd;
	private String spdActSpeed;
	private String spdLd;
	private String spdLd2;
	private String fdOvrd;

	private String mode;
	private String status;

	private String rpdOvrd;
	private String spdOvrd;

	private String mainPrgmName;
	private String mainPrgmStartTime;
	private String mainPrgmStartTime2;
	private String prgmHead;
	private String crntPrgmName;
	
	private String alarmMsg1;
	private String alarmMsg2;
	private String alarmMsg3;
	
	private String alarmNum1;
	private String alarmNum2;
	private String alarmNum3;

	private String mdlM1;
	private String mdlM2;
	private String mdlM3;

	private String mdlT;
	private String mdlD;
	private String mdlH;
	
	private String ioLogik;
	
	private String line;
	private String block;
	private String totalLineNumber;
	private String partCount;
	
	private String seq;
	private String axisLoadZ;
}