package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class AlarmEvtVo{
	  String mcCd;
	  int cntNc;
	  List <AlarmVo> lstNc;
	  int cntPmc;
	  List <AlarmVo> lstPmc;
	  String regdt;
}


