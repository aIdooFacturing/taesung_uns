package com.unomic.dulink.mtc.service;

import java.util.List;

import com.unomic.dulink.mtc.domain.MtcCtrlVo;
import com.unomic.dulink.mtc.domain.SeqVo;
import com.unomic.dulink.mtc.domain.SerialVo;



public interface MtcService {
	public String setChgDvcCtrl(MtcCtrlVo inputVo);
	public void setJsonData(SerialVo inputVo) throws Exception;
	public String addSeq(List<SeqVo> listSeq);
}
