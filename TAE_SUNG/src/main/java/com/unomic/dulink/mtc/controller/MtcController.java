package com.unomic.dulink.mtc.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.mtc.domain.AlarmEvtVo;
import com.unomic.dulink.mtc.domain.MTConnect;
import com.unomic.dulink.mtc.domain.MtcCtrlVo;
import com.unomic.dulink.mtc.domain.RogerParser;
import com.unomic.dulink.mtc.domain.SeqVo;
import com.unomic.dulink.mtc.domain.SerialVo;
import com.unomic.dulink.mtc.service.MtcService;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.adapter.service.AdapterService;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.device.service.DeviceService;
import com.unomic.factory911.mc_prgm.domain.McPrgmVo;
import com.unomic.factory911.mc_prgm.service.McPrgmService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger logger = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private MtcService mtcService;

	@Autowired
	private AdapterService adapterService;

	@Autowired
	private McPrgmService mcPrgmService;
	
	//private HashMap<String,SndVo> mapSndVo = new HashMap<String, SndVo>();
	private MtcShooter mtcShooter = new MtcShooter();
	
	
	@RequestMapping(value = "testPostEcho")
	@ResponseBody
    public String testPostEcho(@RequestBody String strJson, AdapterVo adapterVo) throws Exception{
		logger.error(strJson);
        return "{key:173,name:\"cannon\",rtn:\""+strJson+"\"}";
    }
	
	@RequestMapping(value = "testGetEcho")
	@ResponseBody
    public String testGetEcho(String rtn) throws Exception{
		;
        return "{\"key\":173,\"name\":\"cannon\",\"rtn\":\""+rtn+"\"}";
    }
	
	@RequestMapping(value = "getInitTime")
	@ResponseBody
    public String setInitTime(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "timeTest")
	@ResponseBody
    public String timeTest(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "dbTest")
	@ResponseBody
	public String dbTest(HttpServletRequest request){
		AdapterVo inputVo = new AdapterVo();
		inputVo.setDvcId("7");
		AdapterVo preVo = adapterService.getLastInputData(inputVo);
		logger.info("preVo:"+preVo);
		return "OK";
	}
	
	//�뀒�뒪�듃 �슜�쑝濡� 1踰� �옣鍮꾨쭔 �꽔寃� �릺�뼱 �엳�쓬.
	//�씪�떒臾댁“嫄� �쟾�넚�븯�뒗 濡쒖쭅�쑝濡�.
	//�셿�꽦. �씠踰ㅽ듃 �셿猷� �썑 �쟾�넚.
	//@Scheduled(fixedDelay = 3000)
	public void schSetJsonData(){
		//SndVo rtnVo = mapSndVo.get("1");
		Set <String> keySet = mtcShooter.getMapSrl().keySet();
		Iterator <String> itKey = keySet.iterator();
		
		logger.error("@@@CANNON map size@@@:"+mtcShooter.getMapSrl().size()+"");
		if(mtcShooter.getMapSrl().size()<1){
			return;
		}

		long startTime = System.currentTimeMillis();
		
		if(itKey.hasNext()){
			SerialVo rtnVo = mtcShooter.getMapSrl().get(itKey.next());
			try {
				mtcService.setJsonData(rtnVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		logger.error("##  �냼�슂�떆媛�(珥�.0f) : " + ( endTime - startTime )/1000.0f +"珥�"); 
	}
	
	//�뀒�뒪�듃 �슜�쑝濡� 1踰� �옣鍮꾨쭔 �꽔寃� �릺�뼱 �엳�쓬.
	//�씪�떒臾댁“嫄� �쟾�넚�븯�뒗 濡쒖쭅�쑝濡�.
	//�셿�꽦. �씠踰ㅽ듃 �셿猷� �썑 �쟾�넚.
	//@Scheduled(fixedDelay = 3000)
	public void schSetAlarmData(){
		//SndVo rtnVo = mapSndVo.get("1");
		Set <String> keySet = mtcShooter.getMapSrl().keySet();
		Iterator <String> itKey = keySet.iterator();
		
		logger.error("@@@CANNON map size@@@:"+mtcShooter.getMapSrl().size()+"");
		if(mtcShooter.getMapSrl().size()<1){
			return;
		}
	
		long startTime = System.currentTimeMillis();
		
		if(itKey.hasNext()){
			SerialVo rtnVo = mtcShooter.getMapSrl().get(itKey.next());
			try {
				mtcService.setJsonData(rtnVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long endTime = System.currentTimeMillis();
		logger.error("##  �냼�슂�떆媛�(珥�.0f) : " + ( endTime - startTime )/1000.0f +"珥�"); 
	}
	
	@RequestMapping(value = "setJsonData")
	@ResponseBody
	public void setJsonData(String dvcId){
		SerialVo rtnVo = mtcShooter.getMapSrl().get(dvcId);
		try {
			mtcService.setJsonData(rtnVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@Scheduled(fixedDelay = 1000)
	public void setAlamEvt(){
		SerialVo rtnVo = mtcShooter.getMapSrl().get("1");
		try {
			mtcService.setJsonData(rtnVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Scheduled(fixedDelay = 1000)
//	public void setPrdctCnt(){
//		SndVo rtnVo = mtcShooter.getMapSrl().get("1");
//		try {
//			mtcService.setJsonData(rtnVo);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	

	@RequestMapping(value = "getCrntSrl")
	@ResponseBody
	public SerialVo getCrntSrl(HttpServletRequest request,String dvcId){
		logger.info(""+dvcId);
		SerialVo rtnVo = mtcShooter.getMapSrl().get(dvcId);
		//mapSndVo.
		logger.info("rtnVo:"+rtnVo);
		return rtnVo;
	}
	
	@RequestMapping(value = "getCrntAlm")
	@ResponseBody
	public AlarmEvtVo getCrntAlm(HttpServletRequest request,String dvcId){
		logger.info(""+dvcId);

		AlarmEvtVo rtnVo = mtcShooter.getMapAlamEvt().get(dvcId);
		//mapSndVo.
		logger.info("rtnVo:"+rtnVo);
		return rtnVo;
	}
	
	@RequestMapping(value = "getMapCrnt")
	@ResponseBody
	public SerialVo getMemCrnt(HttpServletRequest request,String dvcId){
		logger.info(""+dvcId);
		SerialVo rtnVo = mtcShooter.getMapSrl().get(dvcId);
		//mapSndVo.
		logger.info("rtnVo:"+rtnVo);
		return rtnVo;
	}
	
	@RequestMapping(value = "pojoTest")
	@ResponseBody
	public String pojoTest(HttpServletRequest request,String dvcId){
		logger.info(""+dvcId);
		SerialVo rtnVo = mtcShooter.getMapSrl().get(dvcId);
		//mapSndVo.
		logger.info("rtnVo:"+rtnVo);
		Gson gson = new Gson();
		String strJson = gson.toJson(rtnVo);
		return strJson;
	}
	
	@RequestMapping(value = "getAllMap")
	@ResponseBody
	public HashMap<String,SerialVo> getAllMap(HttpServletRequest request){
		
		return mtcShooter.getMapSrl();
	}
	
	/*
	 * 	#{dvcId} : �옣鍮� ID 
	 * 	#{chgTy} : 而⑦듃濡� �뜲�씠�꽣 蹂�寃� 醫낅쪟. S/F/D/H 以� 1媛�.
	 * 	#{chgDt} : 蹂�寃� �떊�샇諛쏆� datetime yyyy-MM-dd HH:Mi:ss
	 * */
	@RequestMapping(value = "ctrlChg")
	@ResponseBody
	public String ctrlChg(HttpServletRequest request, MtcCtrlVo inputVo){
		
		String rtnStr = mtcService.setChgDvcCtrl(inputVo);
		
		return rtnStr;
	}
	
	@RequestMapping(value = "seq")
	@ResponseBody
    public String mtcSeq(@RequestBody String mtcCrnt, HttpServletRequest request){
		
		logger.info(mtcCrnt);
		mtcCrnt = mtcCrnt.trim();
		if(mtcCrnt.length()<1){
			return "emptyDvcId";
		}
		
		String [] seq = mtcCrnt.split("/");
		
		ArrayList <String> listSeq = new ArrayList<String> (Arrays.asList(seq));
		
		int firstIdx = 0;
		int dvcId = 0;
		try{
			dvcId = Integer.parseInt(listSeq.get(firstIdx));
		}catch(NumberFormatException e){
			return "parseFailed";
		}
		listSeq.remove(firstIdx);
		
		if(listSeq.size()==0){
			return "emptySeq";
		}
		
		ArrayList<SeqVo> listSeqVo = new ArrayList<SeqVo>();
		
		 for(String s : listSeq){
			 SeqVo tmpVo = new SeqVo();
			 tmpVo.setDvcId(dvcId);
			 logger.info("dvcId:"+tmpVo.getDvcId());
			 tmpVo.setSeq(Integer.parseInt(s));
			 logger.info("seq:"+tmpVo.getSeq());
			 listSeqVo.add(tmpVo);
		 }
		
		String rtnStr = mtcService.addSeq(listSeqVo);
		
		//return rtnStr;
		return rtnStr;
	}
	
	/*
	 * 개발 일자 : 191012 
	 * 알람 받기위한 로직 새로 구현 JSON 형태 
	 * 기존에 가져오지 못하는 알람이 있어 새로 구현함
	 */
	@RequestMapping(value = "mtcAlarm" , method = RequestMethod.POST)
	@ResponseBody
	public String mtcAlarm(@RequestBody String JSONData) {
		String str ="";
		try {
			str = adapterService.mtcAlarm(JSONData);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	/*
	 * 개발 일자 : 191013 
	 * 장비 상태 IN-CYCLE,WAIT,ALARM 인지 확인하는 로직
	 * 기존에 구현되어있는 로직 setAgtChartStatus() 이 있는것 같지만 새로 구현
	 * - Alarm
		    - 알람이 있을 경우 (1순위)
		- Wait
		    - mode 가 ‘MEM’ 또는 ‘MEMORY’ 또는 ‘REMOTE’가 아닐 경우 (2순위)
		    - status 가 ‘STRT’ 또는 ‘START’ 또는 ‘AUTO’가 아닐 경우 (3순위)
		- In-Cycle
		    - 1,2,3 순위의 조건 모두 부합하지 않을 경우 (4순위)
		- CUT
		    - 상태가 In-Cycle 이면서, spindle_load 값이 0 보다 큰 경우
	 */
	
	@RequestMapping(value = "mtcCrnt")
	@ResponseBody
    public String mtcCrnt(@RequestBody String mtcCrnt, HttpServletRequest request){
		
		try {
			mtcCrnt = mtcCrnt.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
			mtcCrnt = URLDecoder.decode(mtcCrnt, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("UTF DECODING FAILED");
			return "UTF_DECODE_FAIL.";
		}
		mtcCrnt = mtcCrnt.trim();
//		logger.info("mtcCrnt : "+mtcCrnt);
//		logger.info("into Crnt case");
		if(mtcCrnt==null){
			logger.error("return null case : no xml Data 0");
			return "XML data is null.";
		}
		if(mtcCrnt.length() < 1){
			logger.error("length < 1 : no xml Data 1["+request.getRemoteAddr()+"]["+mtcCrnt+"]");
			return "no xml Data1";
		}
		String[] result = mtcCrnt.split(" ");
		if("DUPLE".equals(result[0])){
			//logger.info(result[1]);

			//cannon
			//return deviceService.editDvcLastTime(result[1]);
			logger.error("DUPLE:"+result[1]);
			return deviceService.addDvcDuple(result[1]);
			//return "OK";
			
		}

		
		//mtcCrnt = mtcCrnt.replaceAll("[^\\x20-\\x7e]", "");
		List<String> listXML = getListFromInputXML(mtcCrnt);
		
		//logger.info("listXML:"+listXML);
		// mtc xml list 媛� �뱾�뼱媛��꽌 pureStatusVo 媛� �굹���꽌 �씪愿� 泥섎━�븳�떎.

		MTConnect mtConnect = new MTConnect();
		
		//1李� 由ъ뒪�듃 �깮�꽦. 媛�怨듬맂 李⑦듃 �긽�깭 媛믨퉴吏� �뱾�뼱媛�.
		List<AdapterVo> listAdt= new ArrayList<AdapterVo>();

		// First loop
		// set chart status.
		
		logger.info("size Xml:"+listXML.size());
		
		for(int i = 0, size=listXML.size(); i < size ; i++){
			String tmpStr = listXML.get(i);
			
			mtConnect = getMTCfromXML(tmpStr);
			
			logger.info("listXML::::"+mtConnect);

			if(mtConnect==null){
				logger.error("[XML_FORMAT_ERROR]:"+mtcCrnt);
				return "XML_FORMAT_ERROR";
			}
			if(mtConnect.getStatus() == null
				|| mtConnect.getStatus().equals(CommonCode.MSG_UNAVAIL)
				|| mtConnect.getSpdLd().equals("NaN")
				) {
				logger.info("??? :::"+mtConnect);
				//�씠 議곌굔臾몄뿉 遺��빀�븷 寃쎌슦 �뜲�씠�꽣 媛�怨듭씠 �븞�릺誘�濡� 由ы꽩�떆�궓�떎.
				return "Status_IS_UNAVAILABLE:"+mtcCrnt;
			}

//			System.out.println("전에는 :"+mtConnect.getSpdLd2());
			AdapterVo tmpVo = getAGTStatus(mtConnect);
//			System.out.println("후에는 :"+tmpVo.getSpdLd2());
		
			logger.info("tmpVo.getSeq():"+tmpVo.getSeq());
			logger.info("tmpVo.getIoLogik():"+tmpVo.getIoLogik());
			logger.info("getMainPrgmStartTime:"+tmpVo.getMainPrgmStartTime());
			logger.info("getMainPrgmStartTime2:"+tmpVo.getMainPrgmStartTime2());
			logger.info("tmpVo.getSender():"+tmpVo.getSender());
			logger.info("tmpVo.getAlarmMsg1():"+tmpVo.getAlarmMsg1());
			logger.info("tmpVo.getAlarmMsg2():"+tmpVo.getAlarmMsg2());
			logger.info("tmpVo.getAlarmMsg3():"+tmpVo.getAlarmMsg3());
			logger.info("tmpVo.getAxisLoadZ():"+tmpVo.getAxisLoadZ());
			
			listAdt.add(tmpVo);
			
			logger.info("tmpVo.getAxisLoadZ():"+tmpVo.getAxisLoadZ());
			logger.info("tmpVo.getDvcId():"+tmpVo.getDvcId());
			logger.info("CommonFunction.getNowUTC():"+CommonFunction.getNowUTC());
			logger.info("tmpVo.getLine():"+tmpVo.getLine());
			logger.info("tmpVo.getMode():"+tmpVo.getMode());
			logger.info("tmpVo.getStatus():"+tmpVo.getStatus());
			logger.info("tmpVo.getMainPrgmName():"+tmpVo.getMainPrgmName());
			logger.info("tmpVo.getSpdLd():"+tmpVo.getSpdLd());
			logger.info("tmpVo.getSpdLd2():"+tmpVo.getSpdLd2());
			logger.info("tmpVo.getActFd():"+tmpVo.getActFd());
			logger.info("tmpVo.getSpdActSpeed():"+tmpVo.getSpdActSpeed());
			
			try {
				mtcShooter.inputSerial(tmpVo);
				mtcShooter.inputAlarmEvt(tmpVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception e setSendVo@@@:");
				e.printStackTrace();
			}
//			memAdtVo = tmpVo;
			
		}
		
//		String chkStr = setMcInfo(listAdt);
//		if( CommonCode.MSG_RTN_ERROR_PARSE_FAILED.equals(chkStr)){
//			return chkStr; 
//		}
		
//		List<McPrgmVo> listMcPrgm = mcPrgmService.getListMcPrgm();
		
		List<McPrgmVo> listMcPrgm = null;
		for(AdapterVo i: listAdt){
			i = setAgtChartStatus(i,listMcPrgm);
		}

		logger.info("size:"+listAdt.size());
		
		AdapterVo lastVo = listAdt.get(listAdt.size()-1);
		lastVo.setLastIpAddr(request.getRemoteAddr());
		 //留덉�留� �긽�깭 �뾽�뜲�씠�듃 �븯�뒗 荑쇰━.
		//�꽦�뒫 臾몄젣濡� 二쇱꽍.
//		if(deviceService.editLastDvcStatus(lastVo).equals("OK")){
//		}else{
//			return "UPDATE_LAST_DVC_FAIL";
//		}
		
		// dateStarter瑜� �쐞�빐 �삁�쟾 �뜲�씠�꽣 媛��졇�샂.
		// �쁽�옱 以묐났 �젣嫄곕뒗 �븞�븿.
		AdapterVo preVo = adapterService.getLastInputData(lastVo);
		
		logger.info("preVo:"+preVo);
		preVo = setIsZeroStatus(preVo);
		// 由ъ뒪�듃 媛��졇�삤�뒗�뜲源뚯� �솗�씤.
		// 媛� �꽔怨� �긽�깭媛� 鍮꾧탳�븯�뒗�뜲源뚯� �븯硫� 1李� 紐⑺몴 �셿�꽦.(�셿猷�)
		
		// 2李� 紐⑺몴�뒗 �삎踰� �룷�븿�맂 �궗�씠�겢 媛��닔, �깮�궛 媛��닔, 湲몄씠. 
		// �삎踰� �룷�븿�맂 媛�怨� �젙蹂� 泥섎━媛� 怨좊��.
		
		// Second Loop
		// dateStarterSetter.

		for(int i =0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			AdapterVo tmpPreVo;
			AdapterVo dateStarterVo;
			if(i < 1){
				tmpPreVo = preVo;
			}else{
				tmpPreVo = listAdt.get(i - 1);
			}
			dateStarterVo = getDateStarter(tmpPreVo, tmpVo);
			if(dateStarterVo != null){
				listAdt.add(i, dateStarterVo);
			}
		}
		
		//End time Setter
		for(int i =0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			AdapterVo tmpPreVo;

			if(i > 0){
				tmpPreVo = listAdt.get(i - 1);
				tmpPreVo.setEndDateTime(tmpVo.getStartDateTime());
				listAdt.set(i-1, tmpPreVo);
			}
		}
		
		List <DeviceStatusVo> dvcList = new ArrayList <DeviceStatusVo> ();
		
		//int size = listAdt.size();
		for(int i = 0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			DeviceStatusVo crtDvcVo = new DeviceStatusVo();
			
			crtDvcVo.setDvcId(lastVo.getDvcId());
			crtDvcVo.setStartDateTime(tmpVo.getStartDateTime());
			crtDvcVo.setChartStatus(tmpVo.getChartStatus());
			
			crtDvcVo.setSpdLd(tmpVo.getSpdLd());
			crtDvcVo.setSpdLd2(tmpVo.getSpdLd2());
			crtDvcVo.setSpdOvrd(tmpVo.getSpdOvrd());
			
			crtDvcVo.setAlarmNum1(tmpVo.getAlarmNum1());
			crtDvcVo.setAlarmNum2(tmpVo.getAlarmNum2());
			crtDvcVo.setAlarmNum3(tmpVo.getAlarmNum3());

			crtDvcVo.setAlarmMsg1(tmpVo.getAlarmMsg1());
			crtDvcVo.setAlarmMsg2(tmpVo.getAlarmMsg2());
			crtDvcVo.setAlarmMsg3(tmpVo.getAlarmMsg3());
			
			
			if(crtDvcVo.getChartStatus().equals(CommonCode.MSG_DATE_START))
			{continue;}
			
			if(dvcList.size() == 0 ){
				dvcList.add(crtDvcVo);
			}else{//�뿬湲곗꽌 �븷 �옉�뾽. �씠�쟾 �뜲�씠�꽣�� 鍮꾧탳�빐�꽌 chartStatus �떎瑜대㈃ list�뿉 �꽔怨� �븘�땲硫� �꽔�쓣 �븘�슂 �뾾�쓬.
				if(crtDvcVo.getChartStatus().equals(dvcList.get(dvcList.size()-1).getChartStatus())	){
					continue;
				}else{
					dvcList.add(crtDvcVo);
				}
			}
		}
		String strRtn="";

		try {
			strRtn = adapterService.editLastNAddList(listAdt , dvcList);
		} catch (DuplicateKeyException e) {
			// TODO: handle exception
			e.printStackTrace();
			strRtn = "DUPLE";
		}
	
		return strRtn ;
	}
	
	
	private AdapterVo getDateStarter(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			starterVo.setSender(crtVo.getSender());

			//理쒓렐 �뜲�씠�꽣 endTime edit.
			//editLastEndTime(starterVo);
			//DateStarter input.
			//addPureStatus(starterVo);

			return starterVo;
		}else{
			return null;
		}
	}
	
	// 
	//private List<String> getListInputList(String inputXml){
	private List<String> getListFromInputXML(String inputXml){
		
		Scanner sc = new Scanner(inputXml).useDelimiter(CommonCode.MSG_XML_DELIMITER);
		
		ArrayList<String> listXml = new ArrayList<String>();
		
		while(sc.hasNext()){
			String tmp = sc.next()+CommonCode.MSG_XML_DELIMITER;
			logger.info("tmp:"+ tmp);
			listXml.add(tmp.trim());
		}
		
		//Iterator<String> it = listXml.iterator();
		//while(it.hasNext()){logger.info("xml:[" + it.next() + "]");}
//		logger.info("listXml.size():"+listXml.size());
//		logger.info("lastListXml.:"+listXml.get(listXml.size()-1));	
//		listXml.remove(listXml.size()-1);
		
		return listXml;
	}
	
	private AdapterVo getAGTStatus(MTConnect mtc){

		AdapterVo pureStatusVo = new AdapterVo();
		
		Long statusUnixSec = CommonFunction.mtcDateTime2Mil(mtc.getCreationTime());
		
		
		pureStatusVo.setStartDateTime(CommonFunction.unixTime2Datetime(statusUnixSec));
		pureStatusVo.setDvcId(mtc.getSender());
		pureStatusVo.setSender(mtc.getSender());
		
		pureStatusVo.setStatus(mtc.getStatus());
		
		//null 체크
		if(mtc.getAxisLoadZ()==null || mtc.getAxisLoadZ().equals("UNAVAILABLE")) {
			pureStatusVo.setAxisLoadZ((float) 0);
		}else {
			pureStatusVo.setAxisLoadZ(Float.parseFloat(mtc.getAxisLoadZ()));
		}
		
		//null 체크
		if(mtc.getSpdLd2()==null) {
			pureStatusVo.setSpdLd2((float) 0);
		}else {
			pureStatusVo.setSpdLd2(Float.parseFloat(mtc.getSpdLd2()));
		}
		
		if("IOLOGIK".equals(mtc.getStatus())){
			pureStatusVo.setIoLogik(mtc.getIoLogik());
		}else{
			pureStatusVo.setIoLogik("000000");
		}
				
		pureStatusVo.setMainPrgmName(setLimitStr(mtc.getMainPrgmName(),20));
		pureStatusVo.setCrntPrgmName(setLimitStr(mtc.getCrntPrgmName(),20));
		pureStatusVo.setPrgmHead(setLimitStr(mtc.getPrgmHead(),50));
				
		//logger.info("mtc.getAlarmMsg1():"+mtc.getAlarmMsg1());
		
		pureStatusVo.setAlarmMsg1(setLimitStr(mtc.getAlarmMsg1(),100));
		pureStatusVo.setAlarmMsg2(setLimitStr(mtc.getAlarmMsg2(),100));
		pureStatusVo.setAlarmMsg3(setLimitStr(mtc.getAlarmMsg3(),100));

		pureStatusVo.setAlarmNum1(setLimitStr(mtc.getAlarmNum1(),20));
		pureStatusVo.setAlarmNum3(setLimitStr(mtc.getAlarmNum2(),20));
		pureStatusVo.setAlarmNum2(setLimitStr(mtc.getAlarmNum3(),20));
		
		pureStatusVo.setMdlM1(setLimitStr(mtc.getMdlM1(),11));
		pureStatusVo.setMdlM2(setLimitStr(mtc.getMdlM2(),11));
		pureStatusVo.setMdlM3(setLimitStr(mtc.getMdlM3(),11));
		
		pureStatusVo.setMdlT(setLimitStr(mtc.getMdlT(),11));
		pureStatusVo.setMdlD(setLimitStr(mtc.getMdlD(),11));
		pureStatusVo.setMdlH(setLimitStr(mtc.getMdlH(),11));

		if(mtc.getSpdLd() == null
			|| mtc.getSpdLd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
				) {
			pureStatusVo.setSpdLd(null);
		}else{
			pureStatusVo.setSpdLd(Float.parseFloat(mtc.getSpdLd()));
			if (Float.valueOf(mtc.getSpdLd()) == 0 ){
				pureStatusVo.setIsZeroSpdLoad(true);
			}else{ //( Float.valueOf(mtc.getSpindle_load()) > 0 ){
				pureStatusVo.setIsZeroSpdLoad(false);
			}
		}
		
		if(mtc.getFdOvrd() == null
			|| mtc.getFdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setFdOvrd(null);
		}else{
			pureStatusVo.setFdOvrd(Integer.valueOf(mtc.getFdOvrd()).intValue());
			if (Float.valueOf(mtc.getFdOvrd()) == 0 ){
				pureStatusVo.setIsZeroFdOvrd(true);
			}else{// if(Integer.valueOf(mtc.getFdOvrd()) > 0){
				pureStatusVo.setIsZeroFdOvrd(false);
			}
		}
		
		if(mtc.getRpdOvrd() == null
			|| mtc.getRpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
				) {
			pureStatusVo.setRpdFdOvrd(null);
		}else{
			pureStatusVo.setRpdFdOvrd(Integer.parseInt(mtc.getRpdOvrd()));
		}
		
		if(mtc.getSpdOvrd() == null
			|| mtc.getSpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setSpdOvrd(null);
		}else{
			pureStatusVo.setSpdOvrd(Integer.parseInt(mtc.getSpdOvrd()));
		}
		
		if(mtc.getActFd() == null
			|| mtc.getActFd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setActFd(null);
		}else{
			pureStatusVo.setActFd((int)Float.parseFloat(mtc.getActFd()));
			if (Float.valueOf(mtc.getActFd()) == 0 ){
				pureStatusVo.setIsZeroActFd(true);
			}else{// if(Integer.valueOf(mtc.getActual_feed()) > 0){
				pureStatusVo.setIsZeroActFd(false);
			}
		}
		
		if(mtc.getSpdActSpeed() == null
			|| mtc.getSpdActSpeed().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
			) {
			pureStatusVo.setSpdActSpeed(null);
		}else{
			pureStatusVo.setSpdActSpeed((int)(Float.parseFloat(mtc.getSpdActSpeed()) ));
		}

		pureStatusVo.setMode(setLimitStr(mtc.getMode(),11));
		
		
		pureStatusVo.setWorkDate(CommonFunction.mil2WorkDate(statusUnixSec));
		if(mtc.getMainPrgmStartTime() == null
				|| mtc.getMainPrgmStartTime().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime(null);
		}else{
			if(mtc.getMainPrgmStartTime().equals("0")){
				pureStatusVo.setMainPrgmStartTime(null);
			}else{
				pureStatusVo.setMainPrgmStartTime(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime())));
				logger.info("MTC_MainProgramStartTime:"+mtc.getMainPrgmStartTime());
				logger.info("PURE_MainProgramStartTime:"+pureStatusVo.getMainPrgmStartTime());
			}
		}
		if(mtc.getMainPrgmStartTime2() == null
				|| mtc.getMainPrgmStartTime2().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime2(null);
		}else{
			if(mtc.getMainPrgmStartTime2().equals("0")){
				pureStatusVo.setMainPrgmStartTime2(null);
			}else{
				pureStatusVo.setMainPrgmStartTime2(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime2())));
			}
		}
		
		if(mtc.getPartCount() == null
			|| mtc.getPartCount().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getPartCount().equals("NaN")
			) {
			pureStatusVo.setPartCount(null);
		}else{
			pureStatusVo.setPartCount((Integer.parseInt(mtc.getPartCount()) ));
		}
		
		if(mtc.getTotalLineNumber() == null
				|| mtc.getTotalLineNumber().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getTotalLineNumber().equals("NaN")
			) {
			pureStatusVo.setTotalLineNumber(null);
		}else{
			pureStatusVo.setTotalLineNumber((Integer.parseInt(mtc.getTotalLineNumber()) ));
		}
		
		if(mtc.getLine() == null
				|| mtc.getLine().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getLine().equals("NaN")
				) {
				pureStatusVo.setLine(null);
		}else{
			pureStatusVo.setLine((Integer.parseInt(mtc.getLine()) ));
		}
		
		pureStatusVo.setBlock(setLimitStr(mtc.getBlock(),50));
		
		
		return pureStatusVo;
	}
	
	private AdapterVo setAgtChartStatus(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		String status = inputVo.getStatus();
		
		int alarmCnt = getAlarmCnt(inputVo);
		
		inputVo.setAlarmCnt(alarmCnt);
		
//		int mainPrgmCnt = getMainPrgmCnt(inputVo, listPrgm);
//		int pltChgPrgmCnt = getPtlChgPrgmCnt(inputVo, listPrgm);
//		logger.info("mainPrgmCnt:"+mainPrgmCnt);
//		logger.info("pltChgPrgmCnt:"+pltChgPrgmCnt);
		
		if(null != inputVo.getChartStatus() && inputVo.getChartStatus().equals(CommonCode.MSG_DATE_START)){
			//nothing. 
			//logger.info("case1");
		}else if( alarmCnt > 0
				//&& inputVo.getIsZeroFdOvrd()
//				//&& inputVo.getIsZeroSpdLoad()
				//&& ((inputVo.getIsZeroActFd())
//					|| ((inputVo.getModal_m1().equals("5")||inputVo.getModal_m2().equals("5")||inputVo.getModal_m3().equals("5"))
//					|| (inputVo.getModal_m1().equals("05")||inputVo.getModal_m2().equals("05")||inputVo.getModal_m3().equals("05"))))
			)
		{
			inputVo.setChartStatus(CommonCode.MSG_ALARM);
//		}else if( inputVo.getStatus().equals("IOLOGIK") && inputVo.getIoLogik().charAt(2)=='1'){
//			inputVo.setChartStatus(CommonCode.MSG_ALARM);
//		}else if( inputVo.getStatus().equals("IOLOGIK") && inputVo.getIoLogik().charAt(1)=='1'){
//			inputVo.setChartStatus(CommonCode.MSG_WAIT);
//		}else if( inputVo.getStatus().equals("IOLOGIK") && inputVo.getIoLogik().charAt(0)=='1'){
//			inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
//		}else if( inputVo.getStatus().equals("IOLOGIK") && inputVo.getIoLogik().charAt(0)=='0' && inputVo.getIoLogik().charAt(1)=='0' && inputVo.getIoLogik().charAt(2)=='0'){
//			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if(! (inputVo.getMode().equals("MEM") || inputVo.getMode().equals("MEMORY")|| inputVo.getMode().equals("REMOTE") )){
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if(( !( status.equals("STRT") || status.equals("START") || status.equals("AUTO")) ) && alarmCnt < 1){
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		//}else if( "1".equals(inputVo.getModal_m1()) && inputVo.getIsZeroActFd() ){
		//}else if( "1".equals(inputVo.getModal_m1())){
		//	inputVo.setChartStatus(CommonCode.MSG_WAIT);
//		}else if(mainPrgmCnt == 0){
//			inputVo.setChartStatus(CommonCode.MSG_WAIT);
//		}else if(pltChgPrgmCnt != 0){
//			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else {//(status.equals("STRT") || status.equals("START")){
			inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
		}
	
		return inputVo;
	}
	
	private int getMainPrgmCnt(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		int rtnCnt=0;
		for(McPrgmVo i : listPrgm){
			if(CommonCode.MSG_PRGM_TYPE_MAIN.equals(i.getPrgmType()) && inputVo.getMainPrgmName().equals(i.getName()) ){
				rtnCnt++;
			}
		}
		return rtnCnt;
	}
	private int getPtlChgPrgmCnt(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		int rtnCnt=0;
		for(McPrgmVo i : listPrgm){
			if(CommonCode.MSG_PRGM_TYPE_PALLET_CHAGE.equals(i.getPrgmType()) && inputVo.getCrntPrgmName().equals(i.getName()) ){
				rtnCnt++;
			}
		}
		return rtnCnt;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}
		//logger.info("before duple check");
		//logger.info("preVo:"+preVo);
		//logger.info("inputVo:"+inputVo);

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())			
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM3().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}
	
	private String addDeviceChartStatusData(DeviceStatusVo inputVo){

		deviceService.adjustDeviceChartStatus_SP(inputVo);
	
		return "OK";
	}
	
	private AdapterVo setIsZeroStatus(AdapterVo inputVo){
		logger.info("run setIsZeroStatus");
		if (Float.valueOf(inputVo.getSpdLd()) == 0 ){
			inputVo.setIsZeroSpdLoad(true);
		}else{
			inputVo.setIsZeroSpdLoad(false);
		}
		
		if (Integer.valueOf(inputVo.getFdOvrd()) == 0 ){
			inputVo.setIsZeroFdOvrd(true);
		}else{
			inputVo.setIsZeroFdOvrd(false);
		}
		
		logger.info("getActFd:"+inputVo.getActFd());
		if(inputVo.getActFd()==null){
			logger.info("NULL!");
		}

		logger.info("getAxisLoadZ:"+inputVo.getAxisLoadZ());
			
		if (Float.valueOf(inputVo.getActFd()) == 0 ){
			inputVo.setIsZeroActFd(true);
		}else{
			inputVo.setIsZeroActFd(false);
		}
		
		return inputVo;
	}
	
	private int getAlarmCnt(AdapterVo inputVo){
		DeviceVo rtnVo = new DeviceVo();
		
		List<String> listAlarmCode = new ArrayList<String>();
		List<String> listAlarmMsg = new ArrayList<String>();
		
		int rtnCnt=0;
		
		if(! (inputVo.getAlarmNum1() == null || inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum1());
			listAlarmMsg.add(inputVo.getAlarmMsg1());
		}
		
		if(! (inputVo.getAlarmNum2() == null || inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum2());
			listAlarmMsg.add(inputVo.getAlarmMsg2());
		}
		if(! (inputVo.getAlarmNum3() == null || inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum3());
			listAlarmMsg.add(inputVo.getAlarmMsg3());
		}
		
		//fix 160307
		if(listAlarmCode.size() > 0){
			rtnVo.setLastAlarmCode(listAlarmCode.get(listAlarmCode.size()-1));
			rtnVo.setLastAlarmMsg(listAlarmMsg.get(listAlarmMsg.size()-1));
			rtnCnt = listAlarmCode.size();
		}else{
			rtnVo.setLastAlarmCode(CommonCode.MSG_UNAVAIL);
			rtnVo.setLastAlarmMsg(CommonCode.MSG_UNAVAIL);
		}
		return rtnCnt;
	}
	
	private MTConnect getMTCfromXML(String inputXML){
		MTConnect mtConnect = new MTConnect();
		//Only ASCII
		//inputXML = inputXML.replaceAll("[^\\x20-\\x7e]", "");
//		logger.info("inputXML:" + inputXML);
		DocumentBuilder db=null;
		try {

			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Document doc = db.parse(new InputSource(new ByteArrayInputStream(inputXML.getBytes("utf-8"))));
			RogerParser parser = new RogerParser(doc);
			
			parser.Init();
			parser.execute(mtConnect);
			
			logger.info("CRTime:"+mtConnect.getCreationTime());
			logger.info("Sender:"+mtConnect.getSender());
			logger.info("Status:"+mtConnect.getStatus());
			logger.info("FdOvrd:"+mtConnect.getFdOvrd());
			logger.info("RpdFdOvrd:"+mtConnect.getRpdOvrd());
			logger.info("SpdOvrd:"+mtConnect.getSpdOvrd());

			logger.info("MainPrgmName:"+mtConnect.getMainPrgmName());
			logger.info("MainPrgmStrtTime:"+mtConnect.getMainPrgmStartTime());
			logger.info("MainPrgmStrtTime2:"+mtConnect.getMainPrgmStartTime2());
			logger.info("Mode:"+mtConnect.getMode());
			
			logger.info("Mode:"+mtConnect.getMode());
			logger.info("MdlM1:"+mtConnect.getMdlM1());
			logger.info("MdlM2:"+mtConnect.getMdlM2());
			logger.info("MdlM3:"+mtConnect.getMdlM3());
			logger.info("MdlT:"+mtConnect.getMdlT());
			logger.info("MdlD:"+mtConnect.getMdlD());
			logger.info("MdlH:"+mtConnect.getMdlH());
			
			logger.info("AlarmMsg1"+mtConnect.getAlarmMsg1());
			logger.info("AlarmMsg2"+mtConnect.getAlarmMsg2());
			logger.info("AlarmMsg3"+mtConnect.getAlarmMsg3());
			
			logger.info("AlarmNum1"+mtConnect.getAlarmNum1());
			logger.info("AlarmNum2"+mtConnect.getAlarmNum2());
			logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("partCount"+mtConnect.getPartCount());
			logger.info("totalLineNumber"+mtConnect.getTotalLineNumber());
			logger.info("Line"+mtConnect.getLine());
			logger.info("Block"+mtConnect.getBlock());
			
			
			//logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("PrgmHead:"+mtConnect.getPrgmHead());
			logger.info("axisLoadZ:"+mtConnect.getAxisLoadZ());
			logger.info("spdld2:"+mtConnect.getSpdLd2());
			
			
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		return mtConnect;
	}
	
	@RequestMapping(value = "mqt")
	@ResponseBody
	public String mqt(HttpServletRequest request){
		
		String rtnStr = deviceService.mqt();
		
		return rtnStr;
	}
	
	String setLimitStr(String input,int limit){
		String rtnStr;
		if(input == null
				|| input.equals(CommonCode.MSG_UNAVAIL)
				|| input.equals("NaN")
			) {
			rtnStr = null;
		}else{
			int tmpLength = input.length();
			rtnStr = input.substring(0, (tmpLength > limit)?limit:tmpLength);
		}
		
		return rtnStr;
	}

	String setChkNull(String input){
		String rtnStr="";
		if(input == null) { rtnStr = "";}
		return rtnStr;
	}
	
	@RequestMapping(value = "getWifiRequest")
	@ResponseBody
    public String getWifiRequest(){
		return "success"; 
	}
}
