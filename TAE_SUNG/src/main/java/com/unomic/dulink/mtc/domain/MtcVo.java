package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class MtcVo{
	Integer staffId;
	Integer shopId;
	Integer comId;
	String staffName;
	String shopName;
	String position;
	String email;
	String phone;
	String urlStr;
	String imgPath;
	String regId;
	String dvcTk;
}
