package com.unomic.dulink.mtc.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unomic.dulink.mtc.domain.MtcCtrlVo;
import com.unomic.dulink.mtc.domain.SeqVo;
import com.unomic.dulink.mtc.domain.SerialVo;


@Service
@Repository
public class MtcServiceImpl implements MtcService{

	private final String USER_AGENT = "Mozilla/5.0";
	
	private final static String DEVICE_SPACE= "com.factory911.device.";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MtcServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	@Transactional(value="txManager_ma")
	public String addSeq(List<SeqVo> listSeq)
	{	
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		
		dataMap.put("listSeq", listSeq);
		sql_ma.insert(DEVICE_SPACE + "addListSeq", dataMap);

		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String setChgDvcCtrl(MtcCtrlVo inputVo)
	{
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntCtrlChg", inputVo);
		if(cntDvc > 0){
			sql_ma.update(DEVICE_SPACE + "editCtrlChg", inputVo);	
		}else{
			sql_ma.insert(DEVICE_SPACE + "addCtrlChg", inputVo);
		}

		return "OK";
	}
	
	@Override
	// HTTP POST request
	public void setJsonData(SerialVo inputVo) throws Exception {
		String url = "http://112.221.229.27:9000/getJsonData";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		
		Gson gson = new Gson();
		String strJson = gson.toJson(inputVo);
		wr.writeBytes(strJson);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
	}
	

}
