package com.unomic.dulink.mtc.domain;

import java.io.FileReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import common.Logger;

public class RogerParser 
{
	private InputSource is;
	private Document document;
	private XPath xpath;
	private String filename;
	
	public RogerParser(Document doc) {
		this.document = doc;
	}
	
	public void Init() {
		try { 
			xpath = XPathFactory.newInstance().newXPath();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// id濡� 援щ텇�빐�꽌 �뜲�씠�꽣瑜� �꽔�뒗�떎.
	private void InputData(MTConnect con, String name, String data) {
		switch(name) {
		case "actual_feed" : 
			con.setActFd(data);
			break;
		case "spindle_actual_speed" :
			con.setSpdActSpeed(data);
			break;
		case "spindle_load" : 
			con.setSpdLd(data);
			break;
		case "feed_override" : 
			con.setFdOvrd(data);
			break;
		case "mode" : 
			con.setMode(data);
			break;
		case "status" : 
			con.setStatus(data);
			break;
		case "rapid_feed_override" :
			con.setRpdOvrd(data);
			break;
		case "spindle_override" :
			con.setSpdOvrd(data);
			break;
		case "main_prgm_name" :
			con.setMainPrgmName(data);
			break;
		case "main_prgm_start_time" :
			con.setMainPrgmStartTime(data);
		case "main_prgm_start_time2" :
			con.setMainPrgmStartTime2(data);
		case "crnt_prgm_name" :
			con.setCrntPrgmName(data);
			break;
		case "prgm_head" :
			con.setPrgmHead(data);
			break;
		case "alarmNum1" :
			con.setAlarmNum1(data);
			break;
		case "alarmNum2" :
			con.setAlarmNum2(data);
			break;
		case "alarmNum3" :
			con.setAlarmNum3(data);
			break;
		case "alarmMsg1" :
			con.setAlarmMsg1(data);
			break;
		case "alarmMsg2" :
			con.setAlarmMsg2(data);
			break;
		case "alarmMsg3" :
			con.setAlarmMsg3(data);
			break;
		case "modal_m1" :
			con.setMdlM1(data);
			break;
		case "modal_m2" :
			con.setMdlM2(data);
			break;
		case "modal_m3" :
			con.setMdlM3(data);
			break;
		case "IOLogik" :
			con.setIoLogik(data);
			break;
		case "line" :
			con.setLine(data);
			break;
		case "block" :
			con.setBlock(data);
			break;
		case "total_line_count" :
			con.setTotalLineNumber(data);
			break;
//		case "part_count" :
//			con.setPartCount(data);
//			break;
		case "ttl_part_count" :
			con.setPartCount(data);
			break;
		case "t_code" :
			if(data.length()>2) {
				data=data.substring(0, data.length()-2);
			}
			con.setMdlT(data);
			break;
		case "d_code" :
			con.setMdlD(data);
			break;
		case "h_code" :
			con.setMdlH(data);
			break;
		case "axis_load_z" :
			con.setAxisLoadZ(data);
			break;
		case "spindle_load2" :
			if(data.length()>=8) {
				data="0";
			}else {
			}
			con.setSpdLd2(data);
				
//			System.out.println("RogerParser : 스핀들로드 수정::"+data);
		default:
			break;
		}
	}
	
	// �뙆�떛 �떎�뻾
	public void execute(MTConnect con) {
		try {
			NodeList cols = (NodeList)xpath.compile("//MTConnectStreams//Header").evaluate(document, XPathConstants.NODESET);
					
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				con.setCreationTime(cols.item(i).getAttributes().getNamedItem("creationTime").getTextContent()); 
			}
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				con.setSender(cols.item(i).getAttributes().getNamedItem("sender").getTextContent()); 
			}
			
			
			cols = (NodeList)xpath.compile("//MTConnectStreams/Streams/DeviceStream").evaluate(document, XPathConstants.NODESET);
						
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				con.setName(cols.item(i).getAttributes().getNamedItem("name").getTextContent()); 
			}
			cols = (NodeList)xpath.compile("//Samples/*").evaluate(document, XPathConstants.NODESET);
			
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				NamedNodeMap map = cols.item(i).getAttributes();
							
				for(int j = 0; j < map.getLength(); j++) {
					getDataXML(map.item(j), cols.item(i), "actual_feed", con);
					getDataXML(map.item(j), cols.item(i), "spindle_actual_speed", con);
					getDataXML(map.item(j), cols.item(i), "spindle_load", con);
					getDataXML(map.item(j), cols.item(i), "axis_load_z", con);
					getDataXML(map.item(j), cols.item(i), "spindle_load2", con);
					
				}
			}
			//cols = (NodeList)xpath.compile("//Condition/*").evaluate(document, XPathConstants.NODESET);
			cols = (NodeList)xpath.compile("//Events/*").evaluate(document, XPathConstants.NODESET);
			
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				NamedNodeMap map = cols.item(i).getAttributes();
				for(int j = 0; j < map.getLength(); j++) {
					//System.out.println((map.item(j)+ ":" +cols.item(i)));
					getDataXML(map.item(j), cols.item(i), "feed_override", con);
					getDataXML(map.item(j), cols.item(i), "rapid_feed_override", con);
					getDataXML(map.item(j), cols.item(i), "spindle_override", con);

					getDataXML(map.item(j), cols.item(i), "mode", con);
					getDataXML(map.item(j), cols.item(i), "status", con);
					
					getDataXML(map.item(j), cols.item(i), "main_prgm_name", con);
					getDataXML(map.item(j), cols.item(i), "main_prgm_start_time", con);
					getDataXML(map.item(j), cols.item(i), "main_prgm_start_time2", con);
					getDataXML(map.item(j), cols.item(i), "crnt_prgm_name", con);				
					getDataXML(map.item(j), cols.item(i), "prgm_head", con);
					
					getDataXML(map.item(j), cols.item(i), "alarmMsg1", con);
					getDataXML(map.item(j), cols.item(i), "alarmMsg2", con);
					getDataXML(map.item(j), cols.item(i), "alarmMsg3", con);
					
					getDataXML(map.item(j), cols.item(i), "alarmNum1", con);
					getDataXML(map.item(j), cols.item(i), "alarmNum2", con);
					getDataXML(map.item(j), cols.item(i), "alarmNum3", con);					
					
					getDataXML(map.item(j), cols.item(i), "modal_m1", con);
					getDataXML(map.item(j), cols.item(i), "modal_m2", con);
					getDataXML(map.item(j), cols.item(i), "modal_m3", con);

					getDataXML(map.item(j), cols.item(i), "IOLogik", con);
					
					getDataXML(map.item(j), cols.item(i), "line", con);
					getDataXML(map.item(j), cols.item(i), "block", con);
					getDataXML(map.item(j), cols.item(i), "total_line_count", con);
//					getDataXML(map.item(j), cols.item(i), "part_count", con);
					getDataXML(map.item(j), cols.item(i), "ttl_part_count", con);
					getDataXML(map.item(j), cols.item(i), "t_code", con);
					getDataXML(map.item(j), cols.item(i), "d_code", con);
					getDataXML(map.item(j), cols.item(i), "h_code", con);
					
				}
			}
			
			cols = (NodeList)xpath.compile("//Events/*").evaluate(document, XPathConstants.NODESET);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void getDataXML(Node map, Node col, String name, MTConnect con) {
		if(map.getNodeName().equals("name") && map.getNodeValue().equals(name)) {

			InputData(con, name, col.getTextContent());
		}
	}
}