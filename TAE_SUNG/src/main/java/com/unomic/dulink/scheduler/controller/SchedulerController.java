package com.unomic.dulink.scheduler.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.device.service.DeviceService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerController.class);
	
	@Autowired
	private DeviceService deviceService;

//	@Autowired
//	private AdapterService adapterService;

	@RequestMapping(value="spTest")
	@ResponseBody
	public void spTest(){
		DeviceVo dvcVo = new DeviceVo();
		dvcVo.setWorkDate("2015-10-06");
		//deviceService.calcDeviceTimesTest(dvcVo);
		//deviceService.calcDeviceTimes(dvcVo);
		calcDeviceTimes();
	}

	// 100초??
	//@Scheduled(fixedDelay = 100000)
	public void samplingData(){
		deviceService.editDvcLastNoCon_SP(); 
		//Change to whole status times.
		//calcDeviceTimes();
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		calcDvcSummary(setVo);
	}
	//@Scheduled(fixedDelay = 5000)
	public void sndJson(){
		
		logger.info("");
	}

//---- 이하로 안씀	
	//repeat on 10 min.
	//@Scheduled(fixedDelay = 600000)
		public void calcAvg(){
			deviceService.updateMcPrgmAvg();
		}

//	@Scheduled(fixedDelay = 120000)
//	public void syncPop(){
//		popService.syncPop();
//		
//		DeviceVo inputVo = new DeviceVo();
//		inputVo.setTgDate(CommonFunction.unixTime2Datetime(System.currentTimeMillis()));;
//		
//		deviceService.addDvcStatics(inputVo);
//	}

	@RequestMapping(value="dateTest")
	@ResponseBody
	public String dateTest(){
		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;

		return inputVo.getWorkDate();
	}
	
	@RequestMapping(value="timeTest")
	@ResponseBody
	public String timeTest(){

		String tmpTime = CommonFunction.getNowCreationTime();
		return tmpTime;
	}
	
	public String calcDeviceTimes(){
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		logger.info("workDate:"+CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		deviceService.calcDeviceTimes(setVo);
		
		return "OK";
	}

	public void calcOpPf(DeviceVo inputVo){
		deviceService.calcDvcOpPf(inputVo);
	}
	
	public void calcDvcSummary(DeviceVo inputVo){
		deviceService.addDvcSummary(inputVo);
	}
	
	public void addTimeChart(){
		DeviceVo inputVo = new DeviceVo();
		inputVo.setTargetDate(CommonFunction.unixTime2Datetime(System.currentTimeMillis()));
		deviceService.addTimeChart(inputVo);
	}
	
	
	@RequestMapping(value="stampTest")
	@ResponseBody
	public String stampTest(){
		String tmp = "1479712977918";
		//tmp = tmp.substring(0,10);
		Long tmpLong= System.currentTimeMillis();
		//CommonFunction.unixTime2Datetime(Long.parseLong(tmp));
		return CommonFunction.unixTime2Datetime(Long.parseLong(tmp));
	}
	
	/*
	 * stDate,edDate
	 * only 10 min unit.
	 * ex) stDate=2016-01-01 00:00:00&edDate=2016-01-04 23:00:00
	 */
	@RequestMapping(value="batchDvcStatics")
	@ResponseBody
	public String batchDvcStatics(DeviceVo inputVo){
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		logger.info("inputVo.getStDate():"+inputVo.getStDate());
		logger.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	       dateFormat.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
           Date dateSt = null;
           Date dateEd = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  dateEd = dateFormat.parse(inputVo.getEdDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           Calendar cal = Calendar.getInstance();
           

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());

           inputVo.setTgDate(tgDate);
           
           logger.info("stDate:"+inputVo.getTgDate());
           logger.info("edDate:"+edDate);
           
           while(!tgDate.equals(edDate)){
        	   deviceService.addDvcStatics(inputVo);
//        	   try {
//        			Thread.sleep(3000);
//        		}catch(InterruptedException e){
//        			logger.error("sleep exception"); 
//        		}

        	   cal.add(Calendar.MINUTE, 10);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTgDate(tgDate);
               logger.info("tgDate:"+tgDate);
           }
           // EOL
           logger.info("@@@@@@@@@@@@@@@@@@EOL@@@@@@@@@@@@@@@@@@");
           logger.info("stDate:"+tgDate);
           logger.info("edDate:"+edDate);
 
		return "OK";
	}
	
	/*
	 * stDate,edDate
	 * only 10 min unit.
	 * 
	 * batchTimeChart.do?stDate=2017-01-03%2020:28:00.000&edDate=2017-01-04%2013:24:00.000
	 */
	@RequestMapping(value="batchTimeChart")
	@ResponseBody
	public String batchTimeChart(DeviceVo inputVo){
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		logger.info("inputVo.getStDate():"+inputVo.getStDate());
		logger.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	       //DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.zzz");
	       dateFormat.setTimeZone(TimeZone.getTimeZone(CommonCode.TIME_ZONE));
           Date dateSt = null;
           Date dateEd = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  dateEd = dateFormat.parse(inputVo.getEdDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());

           inputVo.setTgDate(tgDate);
           
           logger.info("stDate:"+inputVo.getTgDate());
           logger.info("edDate:"+edDate);

           //CommonFunction.dateTime2Mil(tgDate)
           //while(!tgDate.equals(edDate)){
           while(CommonFunction.dateTime2Mil(tgDate) < (CommonFunction.dateTime2Mil(edDate))){
        	   logger.info("@@@@@@@@@@@@@@@@@@ING@@@@@@@@@@@@@@@@@@");
        	   deviceService.addTimeChart(inputVo);
        	   cal.add(Calendar.MINUTE, 2);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTgDate(tgDate);
               logger.info("tgDate:"+tgDate);
           }
           // EOL
           logger.info("@@@@@@@@@@@@@@@@@@EOL@@@@@@@@@@@@@@@@@@");
           logger.info("edDate:"+tgDate);
           logger.info("edDate:"+edDate);
 
		return "OK";
	}
}

