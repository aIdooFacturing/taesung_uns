package com.unomic.dulink.tool.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.tool.domain.ToolLifeVo;
import com.unomic.dulink.tool.domain.ToolOffsetVo;
import com.unomic.dulink.tool.service.ToolService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/tool")
@Controller
public class ToolController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private ToolService toolSvc;

	
	/*
	 * 	json 으로 받은 데이터 파싱해서 처리 예정.
	 *  hst는 지우고 insert
	 *  mst는 SP 에서 update.
	 * */
	@RequestMapping(value = "addInfo")
	@ResponseBody
	public String ctrlChg(HttpServletRequest request,@RequestBody String strJson){
		
		//ToolVo inputVo = new ToolVo();
		//String rtnStr = toolSvc.setChgDvcCtrl(inputVo);
		LOGGER.error("[addInfo]strJson:"+strJson);
		ArrayList<ToolLifeVo> listLife = json2ToolLife(strJson);
		ArrayList<ToolOffsetVo> listOffset = json2ToolOffset(strJson);
		
		toolSvc.addListToolLife(listLife);
		toolSvc.addListToolOffset(listOffset);
		
		return "OK";
	}
	
	// 파싱 순서.
	// 데이터 가져온다.
	// id, type 가져온다.
	// tool life는 type 관계없이 가져온다.
	// offset은 type에 따라 파싱 다르게 하고 가져온다.
	// 항목 없을경우 null 처리가 피곤하므로 이렇게 처리함.
	private ArrayList<ToolLifeVo> json2ToolLife(String strJson){
		ArrayList<ToolLifeVo> listLife= new ArrayList<ToolLifeVo>();
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;
		try {
			jsonObject = (JSONObject) jsonParser.parse(strJson);
			Integer dvcId = Integer.parseInt(jsonObject.get("DVC_ID")+"");
			String crDt = jsonObject.get("REGDT")+"";
			String workDate = CommonFunction.dateTimeSec2WorkDate(crDt);
			JSONArray listTls = (JSONArray)jsonObject.get("TLS");
			
			for(int i = 0,size = listTls.size(); i < size; i++){
				ToolLifeVo tmpVo = new ToolLifeVo();
				
				JSONObject tool = (JSONObject) listTls.get(i);
				tmpVo.setDvcId(dvcId);
				tmpVo.setCrdt(crDt);
				tmpVo.setDate(workDate);
				
				tmpVo.setGrNo(Integer.parseInt(tool.get("GR_NO")+""));
				tmpVo.setTlNo(Integer.parseInt(tool.get("TL_NO")+""));
				tmpVo.setLife(Integer.parseInt(tool.get("LIFE")+""));
				tmpVo.setCnt(Integer.parseInt(tool.get("CNT")+""));
				
				listLife.add(tmpVo);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listLife;
	}
	
	private ArrayList<ToolOffsetVo> json2ToolOffset(String strJson){
		ArrayList<ToolOffsetVo> listOffset= new ArrayList<ToolOffsetVo>();
		
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject;
		try {
			jsonObject = (JSONObject) jsonParser.parse(strJson);
		
			Integer dvcId = Integer.parseInt(jsonObject.get("DVC_ID")+"");
			String crdt = jsonObject.get("REGDT")+"";
			String workDate = CommonFunction.dateTimeSec2WorkDate(crdt);
			JSONArray listTls = (JSONArray)jsonObject.get("OFS");
			
			String mcTy = jsonObject.get("MC_TY")+"";
			
			if(mcTy.equals("MCT")){
				listOffset = setOfsMct(listOffset,listTls,dvcId,crdt,workDate,1);
			}else if(mcTy.equals("CNC")){
				listOffset = setOfsCnc(listOffset,listTls,dvcId,crdt,workDate,2);
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listOffset;
	}
	
	private ArrayList<ToolOffsetVo> setOfsCnc(ArrayList<ToolOffsetVo> listOffset, JSONArray listTls,Integer dvcId, String crDt, String workDate, int mcTy){
		for(int i = 0,size = listTls.size(); i < size; i++){
			ToolOffsetVo tmpVo = new ToolOffsetVo();
			
			JSONObject tool = (JSONObject) listTls.get(i);
			tmpVo.setDvcId(dvcId);
			tmpVo.setCrdt(crDt);
			tmpVo.setDate(workDate);
			tmpVo.setMcTy(mcTy);
			
			tmpVo.setIdx(Integer.parseInt(tool.get("IDX")+""));
			tmpVo.setGmX(Float.parseFloat(tool.get("GM_X")+""));
			tmpVo.setWrX(Float.parseFloat(tool.get("WR_X")+""));
			tmpVo.setGmY(Float.parseFloat(tool.get("GM_Y")+""));
			tmpVo.setWrY(Float.parseFloat(tool.get("WR_Y")+""));
			tmpVo.setGmZ(Float.parseFloat(tool.get("GM_Z")+""));
			tmpVo.setWrZ(Float.parseFloat(tool.get("WR_Z")+""));
			tmpVo.setGmR(Float.parseFloat(tool.get("GM_R")+""));
			tmpVo.setWrR(Float.parseFloat(tool.get("WR_R")+""));
			tmpVo.setT(Float.parseFloat(tool.get("T")+""));
			
			listOffset.add(tmpVo);
		}
		
		return listOffset;
	}
	
	private ArrayList<ToolOffsetVo> setOfsMct(ArrayList<ToolOffsetVo> listOffset, JSONArray listTls,Integer dvcId, String crDt, String workDate, int mcTy){
		for(int i = 0,size = listTls.size(); i < size; i++){
			ToolOffsetVo tmpVo = new ToolOffsetVo();
			
			JSONObject tool = (JSONObject) listTls.get(i);
			LOGGER.info(tool.toString());
			tmpVo.setDvcId(dvcId);
			tmpVo.setCrdt(crDt);
			tmpVo.setDate(workDate);
			tmpVo.setMcTy(mcTy);
			
			tmpVo.setIdx(Integer.parseInt(tool.get("IDX")+""));
			tmpVo.setGmD(Float.parseFloat(tool.get("GM_D")+""));
			tmpVo.setWrD(Float.parseFloat(tool.get("WR_D")+""));
			tmpVo.setGmH(Float.parseFloat(tool.get("GM_H")+""));
			tmpVo.setWrH(Float.parseFloat(tool.get("WR_H")+""));
			
			listOffset.add(tmpVo);
		}
		
		return listOffset;
	}

	
}
