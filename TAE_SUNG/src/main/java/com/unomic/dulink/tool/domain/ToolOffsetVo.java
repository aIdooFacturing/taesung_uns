package com.unomic.dulink.tool.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class ToolOffsetVo{
	Integer dvcId;
	Integer idx;
	Integer mcTy;
	Float t;
	Float gmH;
	Float wrH;
	Float gmD;
	Float wrD;
	Float gmX;
	Float wrX;
	Float gmY;
	Float wrY;
	Float gmZ;
	Float wrZ;
	Float gmR;
	Float wrR;

	String date;
	String crdt;
	String regdt;
	
	
	
}
