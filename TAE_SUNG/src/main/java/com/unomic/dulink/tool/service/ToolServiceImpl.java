package com.unomic.dulink.tool.service;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.tool.domain.ToolLifeVo;
import com.unomic.dulink.tool.domain.ToolOffsetVo;

@Service
@Repository
public class ToolServiceImpl implements ToolService{

	private final static String TOOL_SPACE= "com.factory911.tool.";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	@Transactional(value="txManager_ma")
	public String addListToolLife(ArrayList<ToolLifeVo> listToolLife)
	{
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("listToolLife", listToolLife);
		sql_ma.insert(TOOL_SPACE + "addListToolLife", dataMap);

		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addListToolOffset(ArrayList<ToolOffsetVo> listToolOffset)
	{
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("listToolOffset", listToolOffset);
		sql_ma.insert(TOOL_SPACE + "addListToolOffset", dataMap);

		return "OK";
	}

}
