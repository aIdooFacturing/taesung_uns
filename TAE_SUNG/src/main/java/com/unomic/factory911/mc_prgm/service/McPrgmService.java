package com.unomic.factory911.mc_prgm.service;

import java.util.List;

import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.mc_prgm.domain.McPrgmVo;



public interface McPrgmService {
	public List<McPrgmVo> getListMcPrgm();
}
