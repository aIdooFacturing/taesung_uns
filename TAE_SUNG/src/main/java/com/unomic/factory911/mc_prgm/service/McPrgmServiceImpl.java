package com.unomic.factory911.mc_prgm.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceSpVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.mc_prgm.domain.McPrgmVo;

@Service
@Repository
public class McPrgmServiceImpl implements McPrgmService{

	
	private final static String MC_PRGM_SPACE= "com.factory911.mc_prgm.";
	
	private static final Logger logger = LoggerFactory.getLogger(McPrgmServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;

	@Override
	public List<McPrgmVo> getListMcPrgm()
	{
		List<McPrgmVo> rtnList = (List<McPrgmVo>) sql_ma.selectList(MC_PRGM_SPACE +"getListMcPrgm");
		
		return rtnList;
	}
}
