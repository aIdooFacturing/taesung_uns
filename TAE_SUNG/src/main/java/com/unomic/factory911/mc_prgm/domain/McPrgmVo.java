package com.unomic.factory911.mc_prgm.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class McPrgmVo{
	Integer id;
	String name;
	Integer avgTimeSec;
	String prgmType;
	String mcType;
}
