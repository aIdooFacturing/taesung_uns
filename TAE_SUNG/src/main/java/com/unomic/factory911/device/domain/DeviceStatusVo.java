package com.unomic.factory911.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceStatusVo{
	String dvcId;
	
	String chartStatus;
	Float spdLd;
	Float spdLd2;
	Integer spdOvrd;
	
	String startDateTime;
	String lastStartDateTime;
	String lastChartStatus;

	String hyung;
	Integer railSize;
	String maType;
	String endDateTime;
	
	
	String alarmMsg1;
	String alarmMsg2;
	String alarmMsg3;
	
	String alarmNum1;
	String alarmNum2;
	String alarmNum3;

	
	int cntData;
}
