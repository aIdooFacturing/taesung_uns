package com.unomic.factory911.adapter.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.parboiled.matchervisitors.IsStarterCharVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;

@Service
@Repository
public class AdapterServiceImpl implements AdapterService{

	private final static String ADAPTER_SPACE= "com.factory911.adapter.";
	private final static String DEVICE_SPACE= "com.factory911.device.";
	private static final Logger logger = LoggerFactory.getLogger(AdapterServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	

	@Override
	public AdapterVo getLastInputData(AdapterVo inputVo){

		AdapterVo newVo = new AdapterVo();
		newVo.setDvcId(inputVo.getDvcId());
		//newVo.setAxisLoadZ(inputVo.getAxisLoadZ());
		Integer cnt =  (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntAdapterStatus",inputVo);
		//Integer cnt =  (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntAdapterStatus",inputVo);
		
		
		 if(0 < cnt){
		// if( cnt == null || cnt != 1 ){
			newVo  = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastAdapterStatus",inputVo);
		}
    	return newVo;
	}
	
	@Override
	public AdapterVo chkDateStarterAGT(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@start Date Time AGT@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

			editLastEndTime(starterVo);
			addPureStatus(starterVo);
			
			return starterVo;
		}
		return null;
		
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);
		//int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntLastAdapterStatus", firstOfListVo);
		if(0 < cnt)
		{
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			tmpAdapterVo.setSender(firstOfListVo.getSender());
			//�씠猷⑦떞�뿉 �븞�뱾�뼱媛�..異뷀썑 蹂댁셿 �븘�슂.?
			if(cnt > 1){
				sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
			}
			
			tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());

			sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
			
		}

		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addPureStatus(AdapterVo pureStatusVo)
	{
		
		logger.info("addPureStatus:"+pureStatusVo.getDvcId());
		sql_ma.insert(ADAPTER_SPACE+"addPureData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addListPureStatus(List<AdapterVo> listPureStatus)
	{	
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		
		dataMap.put("listPureStatus", listPureStatus);
		sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);

		return "OK";
	}
	
	//�몢媛쒖쓽 �떎瑜� 湲곕뒫�씠吏�留� �듃�옖�옲�뀡 �븣臾몄뿉 �븯�굹濡� 臾띠뼱�몺.
	@Override
	@Transactional(value="txManager_ma")
	public String editLastNAddList(List<AdapterVo> listPureStatus, List<DeviceStatusVo> dvcList)
	{
		if(listPureStatus.size()<1){
			logger.error("ZeroSize["+listPureStatus.get(0).getDvcId()+"]");
			return "ZeroSize";
		}
		AdapterVo firstOfListVo = listPureStatus.get(0);
		String mdlTChk = (String) sql_ma.selectOne(ADAPTER_SPACE + "lastmdlTChk", firstOfListVo);	//mdl T 중복 제외하기위해
		String mdlT;
//		System.out.println("최근 모달 값은 ::"+mdlTChk);
		if(mdlTChk==null) {
			mdlT = "9751";
		}else {
			mdlT = mdlTChk;
		}

//		System.out.println("mdlT ?? : " + mdlT);
//		System.out.println("listPureStatus ?? : " + listPureStatus.get(0).getMdlT());
		if(isStringDouble(listPureStatus.get(0).getMdlT()) && isStringDouble(mdlT)) {
//			System.out.println("들어오냐 : " + mdlT);
			if(Integer.parseInt(mdlT)==Integer.parseInt(listPureStatus.get(0).getMdlT())) {	//mdl T가 같은경우
				mdlT = listPureStatus.get(0).getMdlT();
				listPureStatus.get(0).setTCodeChange(null);
			}else {
				mdlT = listPureStatus.get(0).getMdlT();
				listPureStatus.get(0).setTCodeChange(listPureStatus.get(0).getMdlT());
			}
		}else {
			if(mdlT.equals(listPureStatus.get(0).getMdlT())) {
				mdlT = listPureStatus.get(0).getMdlT();
				listPureStatus.get(0).setTCodeChange(null);
			}else {
				mdlT = listPureStatus.get(0).getMdlT();
				listPureStatus.get(0).setTCodeChange(listPureStatus.get(0).getMdlT());
			}
//			System.out.println("여기다해야되나;");
		}
		
		
		
		for(int i=1, len=listPureStatus.size();i<len; i++) {
			/*System.out.println(i);
			System.out.println("이전 ::"+mdlT);
			System.out.println("현재 ::"+Integer.parseInt(listPureStatus.get(i).getMdlT()));
			System.out.println(mdlT==Integer.parseInt(listPureStatus.get(i).getMdlT()));*/
			
			
			//String 아니고 실수 숫자일때만
			if(isStringDouble(mdlT)!=true) {
				mdlT="9751";
			}
			
			if(isStringDouble(listPureStatus.get(i).getMdlT())) {
				
				if(Integer.parseInt(mdlT)==Integer.parseInt(listPureStatus.get(i).getMdlT())) {	//같을경우
					mdlT = listPureStatus.get(i).getMdlT();
					listPureStatus.get(i).setTCodeChange(null);
				}else {
					mdlT = listPureStatus.get(i).getMdlT();
					listPureStatus.get(i).setTCodeChange(listPureStatus.get(i).getMdlT());
				}
			}else {
				if(mdlT.equals(listPureStatus.get(i).getMdlT())) {
					mdlT = listPureStatus.get(i).getMdlT();
					listPureStatus.get(i).setTCodeChange(null);
				}else {
					mdlT = listPureStatus.get(i).getMdlT();
					listPureStatus.get(i).setTCodeChange(listPureStatus.get(i).getMdlT());
				}
			}

		}
		
		//기존 데이터 수량 체크해서 0일 경우에 는 아무것도 안함. 0 이 아니면 udpate.
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);		

		//int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntLastAdapterStatus", firstOfListVo);
		if(cnt > 0){
			//remove Time Exception Data
			//When transaction lock case.
			logger.info(">>>>>LPS STEP1>>>>>"+listPureStatus.get(0));
			
			sql_ma.delete(ADAPTER_SPACE+"removeExceptionData", firstOfListVo);
			
//			if(cnt>1){
//				//sql_ma.update(ADAPTER_SPACE + "editRecoverDataEndTime",tmpAdapterVo);
//				//change to update
//				//sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
//				//�엫�떆諛⑺렪. null �깮湲곕뒗 �씠�쑀 李얠븘�꽌 瑗� �빐寃고빐�빞�븿.
//				sql_ma.update(ADAPTER_SPACE + "chkTmpNullData");
//			}
			
			firstOfListVo.setDvcId(firstOfListVo.getSender());
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			if(tmpAdapterVo!=null){
				logger.info("tmpAdapterVo:"+tmpAdapterVo);
				logger.info("firstOfListVo:"+firstOfListVo);
				tmpAdapterVo.setSender(firstOfListVo.getSender());
				tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
				sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
				//dvcId,,start,,end
			}
		}
		
		
		HashMap <String, List<AdapterVo>> dataMap = new HashMap<String, List<AdapterVo>>();
		dataMap.put("listPureStatus", listPureStatus);
		
		logger.info(">>>>>LPS STEP2>>>>>"+listPureStatus.get(0));
		
		sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);
		
// 170113 cannon �씫�씠 �꼫臾� 嫄몃젮�꽌 �엫�떆濡� 留됯린. �씠�썑濡쒕룄 �븘�슂 �뾾�뒗 遺�遺꾩쑝濡� �삁�긽.	
// �븣�엺 由ъ뒪�듃 �븘�슂�빐�꽌 異붽�. 
		//�씪�씪�씠 �꽔吏� 留먭퀬 �븳踰덉뿉 �엯�젰 諛⑹떇�쑝濡� 蹂�寃�.
		HashMap <String, List<DeviceStatusVo>> dataMapDvc = new HashMap<String, List<DeviceStatusVo>>();
		
		//sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
		
		DeviceStatusVo inputVo = dvcList.get(0);
		
		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
		Iterator <DeviceStatusVo> it =  dvcList.iterator();
		
		if(cntDvc < 1){//���긽 �옣鍮� �늻�쟻媛� �뾾�쓣寃쎌슦 諛붾줈 異붽�.
			dataMapDvc.put("listDvcStatus", dvcList);
			sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
			/*System.out.println("1번이다 ::::::");
			System.out.println(dvcList);
			System.out.println(dataMapDvc);*/
		}else{// 湲곗〈媛� �엳�쓣寃쎌슦 以묐났 泥댄겕�빐�꽌 寃곗젙.
			// 留덉�留� 媛� 李얠븘�샂
			
			DeviceStatusVo lastVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
			logger.info("lastVo:"+lastVo);
			
			// Compare with finalList and dvcList.
			List<DeviceStatusVo> finalList = new ArrayList<DeviceStatusVo>();
			finalList.add(lastVo);
			
			while(it.hasNext()){
				DeviceStatusVo crtDvcVo = it.next();
				if(crtDvcVo.getChartStatus().equals(finalList.get(finalList.size()-1).getChartStatus())	){
					continue;
				}else{
					logger.info("crtDvcVo:"+crtDvcVo);
					finalList.add(crtDvcVo);
				}
			}
			
			finalList.remove(0);
			if(finalList.size()>0){
				dataMapDvc.put("listDvcStatus", finalList);
				sql_ma.update(DEVICE_SPACE + "setEndTmDvcStatus", finalList.get(0) );
				sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
				
/*				System.out.println("2번이다 ::::::");
				System.out.println(dvcList);
				System.out.println(dataMapDvc);
*/				
			}
		}
		
// 理쒖쟻�솕 �븘�슂. 由ъ뒪�듃�븯�굹�떦 荑쇰━媛� �꼫臾� 留롮쓬.?
//		while(it.hasNext()){
//			//sql_ma.update(DEVICE_SPACE +"adjustDeviceChartStatus_SP", it.next());
//			DeviceStatusVo inputVo = it.next();
//			//logger.error("[cannon]inputVo : " + inputVo);
//			
//			int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
//			
//			if(cntDvc < 1){//���긽 �옣鍮� �늻�쟻媛� �뾾�쓣寃쎌슦 諛붾줈 異붽�.
//				sql_ma.insert(DEVICE_SPACE +"addDvcStatus", inputVo );
//			}else{// 湲곗〈媛� �엳�쓣寃쎌슦 以묐났 泥댄겕�빐�꽌 寃곗젙.
//				//case1 �긽�깭 �떎瑜쇨꼍�슦 異붽�.
//				DeviceStatusVo rtnVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
//				//logger.error("[cannon]rtnVo : " + rtnVo);
//				if(! rtnVo.getLastChartStatus().equals(inputVo.getChartStatus())){
//					sql_ma.update(DEVICE_SPACE +"editRecoverDvcStatus", rtnVo );
//					sql_ma.update(DEVICE_SPACE +"editLastDvcStatus", rtnVo );
//					sql_ma.insert(DEVICE_SPACE +"addDvcStatus", inputVo );
//				}
//			}
//		}
		logger.error("Clear insert."+listPureStatus.get(0).getDvcId());
		return "OK";
	}
	
	//문자 숫자 확인하기
	public static boolean isStringDouble(String s) {
		  try {
		   Double.parseDouble(s);
		   return true;
		  } catch (NumberFormatException e) {
		   return false;
		  }
	}
	
	@Override
	public List<AdapterVo> getListWinAgent()
	{
		List<AdapterVo> rtnList  = sql_ma.selectList(ADAPTER_SPACE + "getListWinAgent");
		return rtnList;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM2().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}

	public String editLastDvcStatus(DeviceVo inputVo)
	{
		logger.error("RUN editLastDvcStatus:"+inputVo);
		sql_ma.update(DEVICE_SPACE + "editDvcLastStatus", inputVo);
	
		return "OK";
	}
	
	
	
	private String genJsonAlarm(String strAlarm1, String strAlarm2){
		//private String genJsonAlarm(String alarmCode1, String alarmMsg1, String alarmCode2,String alarmMsg2){
			//[{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"},{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"}]
			logger.info("strAlarm1:"+strAlarm1);
			logger.info("strAlarm2:"+strAlarm2);
			
			JSONArray innerArray = new JSONArray();
			JSONObject innerObject1 = new JSONObject();
			JSONObject innerObject2 = new JSONObject();
			
			String[] arrAlarm1;
			String[] arrAlarm2;
			String alarmMsg1="";
			String alarmCode1="";
			String alarmMsg2="";
			String alarmCode2="";
			if(strAlarm1==null ){

			}else if(!strAlarm1.isEmpty()){
				arrAlarm1 = strAlarm1.split(" ");
				alarmCode1 = arrAlarm1[0];
				alarmMsg1 = arrAlarm1[1];
			}
			if(strAlarm2==null ){
				
			}else if(strAlarm2==null || !strAlarm2.isEmpty()){
				arrAlarm2 = strAlarm2.split(" ");
				alarmCode2 = arrAlarm2[0];
				alarmMsg2 = arrAlarm2[1];
			}

			
			innerObject1.put("ALARM_MSG", alarmMsg1);
			innerObject2.put("ALARM_MSG", alarmMsg2);
			
			innerObject1.put("ALARM_CODE", alarmCode1);
			innerObject2.put("ALARM_CODE", alarmCode2);
			
			innerArray.add(innerObject1);
			innerArray.add(innerObject2);
			
			return innerArray.toString();
		}
		
		private String genJsonModal(String modal){
			String[] arrModal = modal.split(" ");
			List<String> arrM = new ArrayList<String>();
			int size = arrModal.length;
			
			String gCode = "";
			String tCode = "";
			for(int i = 0 ; i<size ; i++){
				
				if(arrModal[i].charAt(0)=='G'){
					gCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='T'){
					tCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='M'){
					arrM.add(arrModal[i].substring(1));
				}
			}

			JSONObject rootObj = new JSONObject();
			JSONArray arrGmodal = new JSONArray();
			JSONObject objG0 = new JSONObject();
			JSONArray arrAuxCode= new JSONArray();
			JSONObject objM1 = new JSONObject();
			JSONObject objM2 = new JSONObject();
			JSONObject objM3 = new JSONObject();
			JSONObject objT = new JSONObject();
			
			objG0.put("G0", gCode); arrGmodal.add(objG0);
			objT.put("T", tCode); arrAuxCode.add(objT);
			
			for(int i = 0 ; i < arrM.size() ; i++){
				if(i==0){
					objM1.put("M1", arrM.get(i)); arrAuxCode.add(objM1);
				}else if(i==1){
					objM2.put("M2", arrM.get(i)); arrAuxCode.add(objM2);
				}else if(i==2){
					objM3.put("M2", arrM.get(i)); arrAuxCode.add(objM3);
				}
			}

			rootObj.put("G_MODAL", arrGmodal);
			rootObj.put("AUX_CODE", arrAuxCode);
			
			return rootObj.toString();
		}

	/*
	 * 2019-10-15
	 * 알람만 받기 위한 로직 추가
	 * 기존에 받아오는 알람이 있지만 새로개발할 FP 알람 (TOOL BROKEN, AIR PRESSURE) 를 받아오지않아
	 * 알람 자체를 받아오는 로직 추가 개발
	 */
		@Override
		public String mtcAlarm(String JSONData) {
			String str="";
//			System.out.println("????");
			try {
				//string => json 형태로 바꾸기
				JSONParser jsonParser = new JSONParser();
				JSONArray jsonArray = (JSONArray) jsonParser.parse(JSONData);

				//Alarm 저장하기위한 List
				List<AdapterVo> AlarmList = new ArrayList<AdapterVo>();
				//Alarm 아닌항목을 저장하기 위한 List
				List<AdapterVo> NormalList = new ArrayList<AdapterVo>();
				
				//dvcId 장비 번호 저장하기
				String dvcId = "";
				
				//중복으로 데이터를 넣지않기위해 추가한 변수
				int alarmCnt = 0;
				int normalCnt = 0;
				
				//이전상태
				String preStatus = "";
				//현재상태
				String status = "";
				
				//받은 데이터 for문 돌려서 alarm 있는거 가지고오기
				for (int i=0; i<jsonArray.size(); i++) {
					AdapterVo adapterVo = new AdapterVo();
					JSONObject tempObj = (JSONObject) jsonArray.get(i);
					
					//alarm list 형태로 들어 오기때문에 list 담기
					ArrayList list = (ArrayList) tempObj.get("alarmList");
					//현재 상태 구하기
					if(list.size()>0) {
						int FPchk = 0;
						
						/*
						 * 2019-10-17
						 * 모든상태의 알람가지고 올려면 
						 * 밑에 FPchk 관련 로직 제거하면 됌
						 * 삭제해야될것 총 2개 중 첫번쨰
						 */

						for(int j=0; j<list.size(); j++) {
							JSONObject FPList = (JSONObject) list.get(j);
							
							//TOOL BROKEN 일경우
							if((FPList.get("message").toString().indexOf("BROKEN")!=-1) && (FPList.get("message").toString().indexOf("TOOL")!=-1)) {
								FPchk ++;
//								System.out.println("== TOOL BROKEN ==");
							}
							//소재착좌
							if((FPList.get("message").toString().indexOf("AIR")!=-1) && (FPList.get("message").toString().indexOf("GAP")!=-1)) {
								FPchk ++;
//								System.out.println("== AIR GAP ==");
							}
							//소재착좌
							if((FPList.get("message").toString().indexOf("AIR")!=-1) && (FPList.get("message").toString().indexOf("PRESSURE")!=-1)) {
								FPchk ++;
//								System.out.println("== AIR PRESSURE ==");
							}
							
						}
						//Fp만 알람으로 하기위해서
						if(FPchk>0) {
							status = "ALARM";
						}else {
							status = "INCYCLE";
						}

					}else {
						status = "INCYCLE";
					}

					//이전 데이터를 비교해야 되므로 첫번쨰 데이터는 따로 로직 구현
					if(i==0) {
						//기존 발생한 알람이 있는지 없는지 확인할 변수
						//cnt == 1 일경우 기존발생한 알람있음 cnt==0일경우 기존발생되어있는 알람 없음
						int cnt = (int) sql_ma.selectOne(ADAPTER_SPACE + "alarmChk" , tempObj.get("dvcId"));
						
						//이전상태구하기
						if(cnt==0) {	
							preStatus = "INCYCLE";
						}else {
							preStatus = "ALARM";
						}
						
//						System.out.println("cnt :" + cnt + " , preStatus :" + preStatus + " , status :" + status);
						//이전상태와 현재상태 비교
						if(!status.equals(preStatus)) {
							//알람 => 가동으로 바꼈을 경우 
							if(preStatus.equals("ALARM")) {
								adapterVo.setDvcId(tempObj.get("dvcId").toString());
								adapterVo.setAlarmMsg1(tempObj.get("alarmList").toString());
								adapterVo.setRegdt(tempObj.get("timeStamp").toString());
								adapterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

								sql_ma.update(ADAPTER_SPACE + "AlarmEndupdate" , adapterVo);
							}
							// 가동 => 알람 으로 바꼈을 경우
							if(preStatus.equals("INCYCLE")) {
								adapterVo.setDvcId(tempObj.get("dvcId").toString());
								adapterVo.setAlarmMsg1(tempObj.get("alarmList").toString());
								adapterVo.setRegdt(tempObj.get("timeStamp").toString());
								adapterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

								sql_ma.insert(ADAPTER_SPACE + "AlarmDatainsert" , adapterVo);
							}
						}
					}else {
						//이전 데이터를 가져
						JSONObject preObj = (JSONObject) jsonArray.get(i-1);
						ArrayList preList = (ArrayList) preObj.get("alarmList");
						
						//과거상태 재정의
						if(preList.size()>0) {
						/*
						 * 2019-10-17
						 * 모든상태의 알람가지고 올려면 
						 * 밑에 FPchk 관련 로직 제거하면 됌
						 * 삭제해야될것 총 2개 중 두번쨰

						 */
							int FPchk = 0;
							for(int j=0; j<preList.size(); j++) {
								JSONObject FPList = (JSONObject) preList.get(j);
								
								//TOOL BROKEN 일경우
								if((FPList.get("message").toString().indexOf("BROKEN")!=-1) && (FPList.get("message").toString().indexOf("TOOL")!=-1)) {
									FPchk ++;
//									System.out.println("== TOOL BROKEN ==");
								}
								//소재착좌
								if((FPList.get("message").toString().indexOf("AIR")!=-1) && (FPList.get("message").toString().indexOf("GAP")!=-1)) {
									FPchk ++;
//									System.out.println("== AIR GAP ==");
								}
								//소재착좌
								if((FPList.get("message").toString().indexOf("AIR")!=-1) && (FPList.get("message").toString().indexOf("PRESSURE")!=-1)) {
									FPchk ++;
//									System.out.println("== AIR PRESSURE ==");
								}
								
							}
							//Fp만 알람하기 위해서
							if(FPchk>0) {
								preStatus = "ALARM";
							}else {
								preStatus = "INCYCLE";
							}
							
//							preStatus = "ALARM";
						}else {
							preStatus = "INCYCLE";
						}
						
//						System.out.println("i :" + i + " , preStatus :" + preStatus + " , status :" + status);


						//현재 상태와 과거 상태가 다를때
						if(!preStatus.equals(status)) {
							//가동 => 알람
							if(preStatus.equals("INCYCLE")) {
								adapterVo.setDvcId(tempObj.get("dvcId").toString());
								adapterVo.setAlarmMsg1(tempObj.get("alarmList").toString());
								adapterVo.setRegdt(tempObj.get("timeStamp").toString());
								adapterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

//								System.out.println("상태달라서 알람발생");
								sql_ma.insert(ADAPTER_SPACE + "AlarmDatainsert" , adapterVo);
							}
							//알람 => 가동
							if(preStatus.equals("ALARM")) {
								adapterVo.setDvcId(tempObj.get("dvcId").toString());
								adapterVo.setAlarmMsg1(tempObj.get("alarmList").toString());
								adapterVo.setRegdt(tempObj.get("timeStamp").toString());
								adapterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

								sql_ma.update(ADAPTER_SPACE + "AlarmEndupdate" , adapterVo);
								
//								System.out.println("수정 버그 222");

							}
							
						}
					}
					
				}
			
			//success 표시후 몇개의 데이터를 보냈는지 체크하기
			//단말 쪽에서 몇개의 데이터를 보낸후 보낸 데이터 수량만큼 삭제하기 위해서 추가 요청
			str = "success " + Integer.toString(jsonArray.size());	
				
			} catch (Exception e) {
				str = "fail";
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			return str;
		}


}
