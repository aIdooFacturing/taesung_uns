package com.unomic.factory911.adapter.service;

import java.util.List;

import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;


public interface AdapterService {
	
	public AdapterVo getLastInputData(AdapterVo inputVo);
	public AdapterVo chkDateStarterAGT(AdapterVo preVo, AdapterVo crtVo);
	public String editLastEndTime(AdapterVo firstOfListVo);
	public String addPureStatus(AdapterVo pureStatusVo);
	public String addListPureStatus(List<AdapterVo> listPureStatus);
	public String editLastNAddList(List<AdapterVo> listPureStatus, List<DeviceStatusVo> dvcList);
	public List<AdapterVo> getListWinAgent();	
	public String mtcAlarm(String JSONData);
}
