package com.unomic.factory911.adapter.domain;

import com.unomic.dulink.common.domain.CommonCode;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AdapterVo{
	
	Long timeStamp;	
	String MD5;
	
	String dvcName;
	String adtIp;
	Integer adtPort;
	String adtname;
	
	String localIp;
	String remoteIp;
	
	String adtLog;
	String logTime;
	String adtStatus;
	String lastHealthTime;
	
	String dvcId;
	
	Long startUnixSec;
	String startDateTime;
	String endDateTime;
	String mode;
	String status;
	String chartStatus;
	String mainPrgmName;
	String crntPrgmName;
	String prgmHead;

	String mainPrgmStartTime;
	String mainPrgmStartTime2;	
	
	Float spdLd;
	Float spdLd2;
	Integer fdOvrd;
	Integer rpdFdOvrd;
	
	Integer spdOvrd;

	Integer actFd;
	Integer spdActSpeed;

	Integer alarmCnt;
	
	String alarmMsg1;
	String alarmMsg2;
	String alarmMsg3;
	
	String alarmNum1;
	String alarmNum2;
	String alarmNum3;

	String workDate;
	
	Boolean isZeroSpdLoad;
	Boolean isZeroFdOvrd;
	Boolean isZeroActFd;
	
	Boolean ioPower;
	Boolean ioInCycle;
	Boolean ioWait;
	Boolean ioAlarm;
	
	String mdlM1;
	String mdlM2;
	String mdlM3;
	
	String tCodeChange;
	String mdlT;
	String mdlD;
	String mdlH;

	String cycleTime;
	String runTime;
	
	String regdt;
	String sender;
	
	Integer tgCnt;
	String seq;

	String ioLogik;
	
	String lastIpAddr;
	
	Integer partCount;
	Integer totalLineNumber;
	Integer line;
	String block; 
	Float axisLoadZ;
	
	public AdapterVo(){
		this.setIoPower(false);
    	this.setIoInCycle(false);
    	this.setIoAlarm(false);
    	this.setIoWait(false);

    	this.setMode("");
    	this.setIsZeroSpdLoad(true);
    	this.setIsZeroFdOvrd(true);
    	this.setIsZeroActFd(true);
    	
    	this.setPrgmHead("");
    	this.setMainPrgmName("");
    	this.setStartDateTime("2015-09-25 17:00:00.000");
    	this.setChartStatus(CommonCode.MSG_STAT_INIT);
    	
    	this.spdLd = (float) 0;
    	this.spdLd2 = (float) 0;
    	this.fdOvrd = 0;
    	this.rpdFdOvrd = 0;
    	this.spdOvrd = 0;
    	this.actFd =  0;
    	this.spdActSpeed = 0;
    	
    	this.partCount = null ;
    	this.totalLineNumber = null;
    	this.line = null;
    	this.block = null;
    	this.axisLoadZ = (float) 0;
    	
    	this.setAlarmMsg1(CommonCode.MSG_UNAVAIL);
    	this.setAlarmMsg2(CommonCode.MSG_UNAVAIL);
    	this.setAlarmMsg3(CommonCode.MSG_UNAVAIL);

    	this.setAlarmNum1(CommonCode.MSG_UNAVAIL);
    	this.setAlarmNum2(CommonCode.MSG_UNAVAIL);
    	this.setAlarmNum3(CommonCode.MSG_UNAVAIL);

    	this.setMdlM1(CommonCode.MSG_UNAVAIL);
    	this.setMdlM2(CommonCode.MSG_UNAVAIL);
    	this.setMdlM3(CommonCode.MSG_UNAVAIL);

    	this.setTCodeChange(CommonCode.MSG_UNAVAIL);
    	this.setMdlT(CommonCode.MSG_UNAVAIL);
    	this.setMdlD(CommonCode.MSG_UNAVAIL);
    	this.setMdlH(CommonCode.MSG_UNAVAIL);
    	
    	this.setLastIpAddr(null);
    	
	}
}
