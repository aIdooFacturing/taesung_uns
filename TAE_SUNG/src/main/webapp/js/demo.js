$(function(){
	setEl();
	bindEvt();
	drawMachine();
	drawMachineThumb()
});

function drawMachine(){
	var machine = document.createElement("div");
	machine.setAttribute("class", "machine");
	machine.setAttribute("id", "m1");
	machine.style.cssText = "position : absolute;" + 
							"width : " + getElSize(300) + ";" + 
							"height : " + getElSize(300) + ";" + 
							"background-color : green;" + 
							"left : " + ((contentWidth/2) - getElSize(150)) + "; " +
							"top : " + ((contentHeight/2) - getElSize(150)) + "; " + 
							"border-radius : 50%";
	
	$("#slide1").append(machine);
};

function drawMachineThumb(){
	var machine = document.createElement("div");
	machine.setAttribute("class", "machine");
	machine.setAttribute("id", "m1");
	machine.style.cssText = "position : absolute;" + 
							"width : " + getElSize(30) + ";" + 
							"height : " + getElSize(30) + ";" + 
							"background-color : green;" + 
							"left : " + (($("#l1").width()/2) - getElSize(15)) + "; " +
							"top : " + (($("#l1").height()/2) - getElSize(15)) + "; " + 
							"border-radius : 50%";
	
	$("#l1").append(machine);
};

function bindEvt(){
	$("#addLayout").click(addLayout);
	$(".layout").click(selectLayout);
	$(".layout").mouseover(showDelBtn);
	$(".layout").mouseout(hideDelBtn);
};

function selectLayout(){
	$(".layout").css({
		"border" : "none",
		"top" : (($("#nav").height()/2) - ($(".layout").height()/2))
	});
	
	$(this).css({
		"border" : getElSize(10) + "px solid rgb(59,159,251)",
		"top" : (($("#nav").height()/2) - ($(".layout").height()/2)) - getElSize(10)
	});
	
	$(".slide").css({
		"z-index" : -1
	});
	
	var slideId = "slide" + this.id.substr(1);

	$("#" + slideId).css({
		"z-index" : 1
	});
};

function addLayout(){
	clearInterval(del_btn_Interval);
	del_btn_focus = false;
	
	//add thumb
	var div  = document.createElement("div");
	div.setAttribute("class", "layout");
	div.setAttribute("id","l" + (Number($(".layout").last().attr("id").substr(1))+1));
	
	div.style.cssText = "position : absolute;" + 
						"width : " + $(".layout").width() + ";" + 
						"height : " + $(".layout").height() + ";" + 
						"background-color : " + $("#content").css("background-color") + ";" + 
						"cursor : pointer;" + 
						"z-index : 2;" +
						"top : " + (($("#nav").height()/2) - ($(".layout").height()/2)) + ";" + 
						"left : " + $("#nav").width();
	
	$("#nav").append(div);
	
	$(div).click(selectLayout);
	$(div).mouseover(showDelBtn);
	$(div).mouseout(hideDelBtn);
	
	var margin = getElSize(100);
	
	$(".layout").each(function(idx, data){
		$(data).animate({
			"left" : (($("#nav").width()/2) - ((($(data).width() + margin)/2) * ($(".layout").length)))  + (idx * ($(data).width() + margin))
		}, 1000)
	});
	
	//add slide
	var slide = document.createElement("div");
	slide.setAttribute("id","slide" + ($(".layout").last().attr("id").substr(1)));
	slide.setAttribute("class", "slide");
	slide.style.cssText = "width : " + contentWidth + " ;" + 
							"height : " + contentHeight + " ;" + 
							"background-color : rgb(16,18,20);" + 
							"position : absolute;";
	
	$("#content").append(slide);
};

var del_btn_Interval = null;
var del_btn_flag = false;
var del_btn_focus = false;
function showDelBtn(){
	var id = "del_btn" + this.id.substr(1);
	var layout_id = this.id;
	
	del_btn_Interval = setInterval(function(){
		var img = document.createElement("img");
		img.setAttribute("src", ctxPath + "/images/close_btn.png");
		img.setAttribute("id", id);
		img.setAttribute("class", "del_btn");
		
		img.style.cssText = "width:" + getElSize(80) + ";" + 
							"position : absolute;" + 
							"z-index : 9; " + 
							"top:0;" + 
							"left:0;";
		
		if(!del_btn_flag){
			$("#" + layout_id).append(img);
			del_btn_flag = true;
		};
		
		$(img).click(delLayout);
		$(img).mouseover(function(){
			del_btn_focus = true;
		});
		
		$(img).mouseout(function(){
			del_btn_focus = false;
		});
		
	}, 1000) ;
};

function delLayout(){
	clearInterval(del_btn_Interval);
	
	var id = "l" + this.id.substr(7);
	var slideId = "slide" + this.id.substr(7);
	$("#" + this.id).remove();
	$("#" + id).remove();
	$("#" + slideId).remove();
	
	var margin = getElSize(100);
	$(".layout").each(function(idx, data){
		$(data).animate({
			"left" : (($("#nav").width()/2) - ((($(data).width() + margin)/2) * ($(".layout").length)))  + (idx * ($(data).width() + margin))
		}, 700)
	});
	
	del_btn_flag = false;
};

function hideDelBtn(){
	var id = "del_btn" + this.id.substr(1);
	
	setTimeout(function(){
		if(!del_btn_focus){
			$("#" + id).remove();
			del_btn_flag = false;
		}
	}, 500);
	
	clearInterval(del_btn_Interval);
	del_btn_flag = false;
};

function setEl(){
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	$("body").css({
		"background-color" : "black"
	});
	
	$("#content").css({
		"width" : contentWidth,
		"height" : contentHeight,
		"background-color" : "rgb(16,18,20)",
		"position" : "absolute",
		"left" : (width/2) - (contentWidth/2),
		"top" : (height/2) - (contentHeight/2),
	});
	
	$(".slide").css({
		"width" : contentWidth,
		"height" : contentHeight,
		"background-color" : "rgb(16,18,20)",
		"position" : "absolute",
		"z-index" : 1
	});
	
	$("#nav").css({
		"position" : "absolute",
		"background-color" : "white",
		"width" : contentWidth,
		"height" : getElSize(300),
		"overflow" : "auto",
		"z-index" : 5
	});
	
	$("#addLayout").css({
		"position" : "absolute",
		"font-size" : getElSize(150),
		"cursor" : "pointer",
		"z-index" : 2,
	});
	
	$("#addLayout").css({
		"top" : ($("#nav").height()/2) - ($("#addLayout").height()/2),
		"right" : getElSize(50)
	});
	
	$("#l1").css({
		"position" : "absolute",
		"width" : contentWidth * 0.1,
		"height" : contentHeight * 0.1,
		"border" : getElSize(10) + "px solid rgb(59,159,251)",
		"background-color" : $("#content").css("background-color"),
		"cursor" : "pointer"
	});
	
	$("#l1").css({
		"top" : ($("#nav").height()/2) - ($("#l1").height()/2) - getElSize(10),
		"left" : ($("#nav").width()/2) - ($("#l1").width()/2) - getElSize(10)
	});
};